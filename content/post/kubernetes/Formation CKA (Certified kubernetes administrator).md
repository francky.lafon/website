---
title: "Kubernetes"
description: "Description"
date: 2022-02-13
draft: true
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: image.
categories:
 - Devops
---

## Introduction

Kubernetes est un système open source de gestion de conteneurs développé par Google. Il a été créé en 2014 pour aider à la gestion des applications déployées sur des centaines ou des milliers de nœuds de calcul dans le cloud.

Kubernetes aide à automatiser la distribution, la mise en œuvre et la gestion des conteneurs d'applications. Cela signifie que les administrateurs peuvent définir les ressources nécessaires pour leur application, telles que les quantités de CPU et de mémoire, et laisser Kubernetes gérer la distribution des conteneurs dans le cluster. De plus, Kubernetes peut également surveiller les applications en cours d'exécution et effectuer des actions telles que la récupération automatique en cas d'erreur.

Kubernetes offre une grande flexibilité et une gestion centralisée des applications, ce qui permet aux équipes d'informatique de déployer, gérer et surveiller efficacement des applications à grande échelle. De plus, Kubernetes est compatible avec un large éventail de technologies, ce qui permet aux développeurs de choisir les outils et les technologies qui leur conviennent le mieux.

En résumé, Kubernetes est un outil de gestion de conteneurs hautement automatisé qui aide les équipes informatiques à déployer, gérer et surveiller efficacement des applications à grande échelle.

## kubernetes definition

Kubernetes est une plateforme portable, extensible et open-source pour gérer les charges de travail et les services containérisés, qui facilite la configuration déclarative et l'automatisation.

## kubernetes controle plane

Il y a plusieurs composants dans un "noeud master" qui sont essentiel au bon fonctionnement du cluster

- kube-apiserver est un composant central de Kubernetes qui fournit une API REST pour gérer les objets de configuration du système tels que les pods, les services, les déploiements, etc. Il s'agit de la première porte d'entrée pour les utilisateurs et les contrôleurs qui souhaitent interagir avec le cluster Kubernetes. Le kube-apiserver s'exécute sur le noeud principal du cluster et peut être configuré pour s'authentifier et s'autoriser avec des stratégies définies par l'administrateur du cluster. Il stocke également les informations de configuration du cluster dans un backend de persistance, comme etcd. En résumé, le kube-apiserver est un élément clé pour garantir la disponibilité et la fiabilité du cluster Kubernetes.

- etcd est un système de stockage de données distribuées et fiables, qui est utilisé pour stocker la configuration de Kubernetes et les états des objets dans le cluster. Il assure la persistance de ces données même en cas de défaillance d'un noeud et permet une réplication de ces données à travers plusieurs nœuds pour assurer la disponibilité. etcd est responsable de maintenir la cohérence des données et garantit la consistance des données dans le cluster en utilisant le consensus Raft. Les API RESTful exposées par etcd permettent à kube-api-server d'interagir avec les données du cluster, tandis que etcd s'occupe de la mise en œuvre de la gestion de la persistance de ces données. En somme, etcd est un composant clé pour garantir la cohérence des données dans le cluster Kubernetes.

- Le kube-scheduler est un composant fondamental de Kubernetes qui fonctionne en collaboration avec d'autres composants du système pour assigner les pods (groupes de containers) à des nœuds dans le cluster. Il effectue cette tâche en utilisant un algorithme de planification qui tient compte de divers critères tels que les ressources nécessaires pour exécuter les pods, les restrictions d'affinité, les préférences d'emplacement et la qualité de service. Le kube-scheduler envoie les décisions de planification à l'API Server, qui les enregistre dans la base de données ETCD. Les nœuds Kubernetes vérifient régulièrement ces informations pour déterminer les pods qu'ils doivent exécuter.

- Le kube-controller-manager est un composant de contrôleur de niveau supérieur dans le système Kubernetes. Il s'occupe de la gestion des contrôleurs tels que les contrôleurs de réplicasets, les contrôleurs de déploiements, les contrôleurs d'horodatage, etc. Il a pour but de surveiller l'état du cluster et d'apporter les corrections nécessaires pour atteindre et maintenir l'état souhaité. Par exemple, si un nombre de réplicas d'un Pod spécifique ne correspond pas à ce qui est déclaré dans la configuration, le kube-controller-manager prend des mesures pour rétablir l'état souhaité en créant ou en supprimant des réplicas. Le kube-controller-manager est un composant clé pour maintenir la stabilité et l'intégrité du cluster Kubernetes.

- Le cloud-controller-manager est un composant de Kubernetes qui permet de gérer les fonctionnalités de contrôleur de niveau cloud. Il s'exécute en tant que processus dédié sur chaque nœud du cluster et agit en tant que point central pour les opérations de contrôleur liées à l'infrastructure cloud, telles que la gestion des ressources dans les services de stockage de fichiers en nuage, la configuration du réseau, l'attribution d'adresses IP et les réglages de sécurité. Le cloud-controller-manager s'assure que les ressources du cluster Kubernetes sont automatiquement provisionnées, mises à jour et gérées de manière efficace et cohérente sur l'infrastructure cloud sous-jacente.

## kubernetes nodes

- Kubelet est un composant de nœud de Kubernetes qui exécute sur chaque nœud de cluster et assure que les conteneurs déclarés dans les pods sont en cours d'exécution et en bonne santé. Il communique avec le contrôleur de cluster, notamment avec le API server de Kubernetes, pour obtenir les informations sur les déploiements et les tâches planifiées pour le nœud. Le kubelet assure également que les images de conteneur nécessaires sont téléchargées sur le nœud.

- Kube-proxy est un composant de nœud de Kubernetes qui agit comme un proxy réseau pour les services de cluster. Il garantit que les connexions réseau à un service aboutissent au nœud approprié avec les pods exécutant les travaux correspondants. Kube-proxy utilise l'API de Kubernetes pour déterminer les informations sur les services et les destinations réseau associées, et met en place des règles de routing pour que les connexions au service soient redirigées vers les pods correspondants.

## Core concept

### kubelet

- register node
- create PODs
- monitor node et pods

`ps -aux | grep kubelet` pour avoir les info

### kube-proxy

connexion entre les pods par les services
tout les nodes on un service kube-proxy

`k get daemonset -n kube-system`

### recap pod

`k run nginx --image nginx`

### pod with YAML

pod:v1
service: v1
replicaset: apps/v1
deployment: apps/v1

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: toto-pod
  labels:
    app: toto
    type: frontend
spec:
  containers: #list/array
  - name: nginx-container
    image: ngnix
```

k create -f toto.yml

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: toto-pod
  labels:
    app: toto
    tier: frontend
spec:
  containers:
  - name: nginx-container
    image: ngnix
```

### Replication controller

replication controller =/ replicat set

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: toto-pod
  labels:
    app: toto
    tier: frontend
spec:
    template:
        metadata:
            name: toto-pod
            labels:
                app: toto
                tier: frontend
            spec:
            containers:
            - name: nginx-container
              image: ngnix
          metadata:
            name: toto-pod
            labels:
                app: toto
                tier: frontend
            spec:
            containers:
            - name: nginx-container
                image: ngnix
    replicas: 3
```

```yaml
apiVersion: apps/v1
kind: ReplicatSet
metadata:
  name: toto-pod
  labels:
    app: toto
    tier: frontend
spec:
  template:
    metadata:
      name: toto-pod
      labels:
        app: toto
        tier: frontend
      spec:
        containers:
        - name: nginx-container
          image: ngnix
      metadata:
        name: toto-pod
        labels:
          app: toto
          tier: frontend
      spec:
        containers:
        - name: nginx-container
          image: ngnix
  replicas: 3
    selector:
      matchLabels:
        type: frontend
```

### Labels and selectors

k remplace -f file
k scale --replicas=6 -f file
k scale --replicas=6 replicaset toto-pod

### deployments

replicaset est dans deployments

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: toto-pod
  labels:
    app: toto
    type: frontend
spec:
  template:
    metadata:
      name: toto-pod
      labels:
        app: toto
        tier: frontend
    spec:
      containers:
      - name: nginx-container
        image: ngnix
    replicas: 3
    selector:
      matchLabels:
        type: frontend
```

### certification tips

k run pour générer un template yaml

Reference (Bookmark this page for exam. It will be very handy):

<https://kubernetes.io/docs/reference/kubectl/conventions/>

Create an NGINX Pod

kubectl run nginx --image=nginx

Generate POD Manifest YAML file (-o yaml). Don't create it(--dry-run)

kubectl run nginx --image=nginx --dry-run=client -o yaml

Create a deployment

kubectl create deployment --image=nginx nginx

Generate Deployment YAML file (-o yaml). Don't create it(--dry-run)

kubectl create deployment --image=nginx nginx --dry-run=client -o yaml

Generate Deployment YAML file (-o yaml). Don't create it(--dry-run) with 4 Replicas (--replicas=4)

kubectl create deployment --image=nginx nginx --dry-run=client -o yaml > nginx-deployment.yaml

Save it to a file, make necessary changes to the file (for example, adding more replicas) and then create the deployment.

kubectl create -f nginx-deployment.yaml

OR

In k8s version 1.19+, we can specify the --replicas option to create a deployment with 4 replicas.

kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml

### Services

ouverture du port du container

Service Type

- NodePort

- ClusterIP

- Loadbalancer

NodePort

1. targetport et le port du container
2. port du service côté container
3. Nodeport et le port extérieur

```yaml
apiVersion: apps/v1
kind: Service
metadata:
  name: toto-pod

spec:
  type: NodePort
  ports:
    - targetPort: 80
      port: 80
      nodePort: 3008
  selector:
    app: toto
    type: frontend
```

Si plusieurs pod algorithme: random
sessionAffinty: yes

si multi nodes: toutes les ip des nodes sont bonne

### Service clusterIP

```yaml
apiVersion: apps/v1
kind: Service
metadata:
  name: backend

spec:
  type: ClusterIP
  ports:
    - targetPort: 80 # expose
      port: 80 # pod
  selector:
    app: toto
    type: backend
```

### Service Loadbalancer

Que sur du cloud GCP

```yaml
apiVersion: apps/v1
kind: Service
metadata:
  name: backend

spec:
  type: LoadBalancer
  ports:
    - targetPort: 80 # expose
      port: 80 # pod
      nodePort: 3008
```

### Namespace

kube-public c'est accessible pour tout le monde

pour avoir une ressource d'un autre ns db-service.dev.svc.cluster.local dev est l'autre ns
cluster.local domaine
svc service
db-service le nom du service

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: toto-pod
  labels:
    app: toto
    type: frontend
spec:
  containers: #list/array
  - name: nginx-container
    image: ngnix
```

resource quota

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: toto-pod
  namespace: dev
spec:
  hard:
    pods: "10"
    requests.cpu: "4"
    requests.memory: 5Gi
    limits.cpu: "10"
    limits.memory: 10Gi
```

### imperative vs declarative

imperative -> par ligne de commande
`k create -f`
`k remplace`
declarative -> k apply -f

pour l'exam il vaut mieux faire de l’impératif pour être plus rapide

Certification Tips - Imperative Commands with Kubectl
While you would be working mostly the declarative way - using definition files, imperative commands can help in getting one time tasks done quickly, as well as generate a definition template easily. This would help save considerable amount of time during your exams.

Before we begin, familiarize with the two options that can come in handy while working with the below commands:

--dry-run: By default as soon as the command is run, the resource will be created. If you simply want to test your command , use the --dry-run=client option. This will not create the resource, instead, tell you whether the resource can be created and if your command is right.

-o yaml: This will output the resource definition in YAML format on screen.

Use the above two in combination to generate a resource definition file quickly, that you can then modify and create resources as required, instead of creating the files from scratch.

POD
Create an NGINX Pod

kubectl run nginx --image=nginx

Generate POD Manifest YAML file (-o yaml). Don't create it(--dry-run)

kubectl run nginx --image=nginx --dry-run=client -o yaml

Deployment
Create a deployment

kubectl create deployment --image=nginx nginx

Generate Deployment YAML file (-o yaml). Don't create it(--dry-run)

kubectl create deployment --image=nginx nginx --dry-run=client -o yaml

Generate Deployment with 4 Replicas

kubectl create deployment nginx --image=nginx --replicas=4

You can also scale a deployment using the kubectl scale command.

kubectl scale deployment nginx --replicas=4

Another way to do this is to save the YAML definition to a file and modify

kubectl create deployment nginx --image=nginx --dry-run=client -o yaml > nginx-deployment.yaml

You can then update the YAML file with the replicas or any other field before creating the deployment.

Service
Create a Service named redis-service of type ClusterIP to expose pod redis on port 6379

kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml

(This will automatically use the pod's labels as selectors)

Or

kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml (This will not use the pods labels as selectors, instead it will assume selectors as app=redis. You cannot pass in selectors as an option. So it does not work very well if your pod has a different label set. So generate the file and modify the selectors before creating the service)

Create a Service named nginx of type NodePort to expose pod nginx's port 80 on port 30080 on the nodes:

kubectl expose pod nginx --type=NodePort --port=80 --name=nginx-service --dry-run=client -o yaml

(This will automatically use the pod's labels as selectors, but you cannot specify the node port. You have to generate a definition file and then add the node port in manually before creating the service with the pod.)

Or

kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run=client -o yaml

(This will not use the pods labels as selectors)

Both the above commands have their own challenges. While one of it cannot accept a selector the other cannot accept a node port. I would recommend going with the kubectl expose command. If you need to specify a node port, generate a definition file using the same command and manually input the nodeport before creating the service.

Reference:
<https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands>

<https://kubernetes.io/docs/reference/kubectl/conventions/>

## Schedule

## notes

--dry-run -o yaml
--record

RBAC role c'est pour les namespace
clusterrole c'est global

serviceaccount ??

k top à tester

lab building a kubernetes 1.24 cluster with kubeadm
