---
title: "Kubernetes cli"
description: "Comment utilisé k8s?"
date: "2023-02-13"
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: kubernetes.jpg
categories:
 - Système
 - Devops
---


Hello,

Dans cette article je vais vous parler des commandes qui sont utilisé dans kubernetes! pour l'installer, j'ai fait un role qui réalise la mise en place d'un cluster avec un controle plane et plusieurs worker: <https://gitlab.com/francky.lafon/ansible>. Si vous voulez des informations sur les composants de k8s, j'ai fait un autre article sur l’aspect design.

PS: `alias k="kubectl"`

## Commande sur le cluster

``` bash
k get nodes # Pour voir l'état de santé des noeud kubernetes
```

## Commande sur les namespaces

Par default, on est sur le namespace default.

``` bash
k get namespace # voir tout les namespaces
k create namespace toto # créé un namespace
k config set-context --current --namespace=toto # changer de namespace
```

## Quelles sont les commandes de base à connaître ?

``` bash
k get pods
k get service
k get persistantevolume
k get PersistanteVolumeClaim
k describe # Pour avoir le plus d'information sur la ressource demandé.
```

## Helm

## Gestion des certificats

## Gestion du réseau

## Ingress contrôleur

## Benchmark sécurité

## Gestion des nodes ?
