---
title: "Kubernetes Master"
description: "Etre dans la tête de kube"
date: 2023-02-13
draft: true
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: image.
categories:
 - Système
 - Other
 - Réseau
---

### operate ETCD

Le service ETCD est un magasin clé-valeur qui écoute sur le port 2379. il y a 2 versions.

La version 2 prend en charge les commandes suivantes:

```bash
etcdctl backup
etcdctl cluster-health
etcdctl mkdir
etcdctl set
```

Tandis que la commande sont différentes dans la version 3

``` bash
etcdctl snapshot save
etcdctl endpoint health
etcdctl get
etcdctl put
```

Pour définir la bonne version de l'API, définissez la variable d'environnement ETCDCTL_API avec la commande suivante:

``` bash
export ETCDCTL_API=2|3
```

Lorsque la version de l'API n'est pas définie, elle est considérée comme définie à la version 2. Et les commandes de la version 3 listées ci-dessus ne fonctionnent pas. Lorsque la version de l'API est définie à la version 3, les commandes de la version 2 listées ci-dessus ne fonctionnent pas.

En plus de cela, vous devez également spécifier le chemin vers les fichiers de certificat pour que ETCDCTL puisse s'authentifier auprès du serveur API ETCD. Les fichiers de certificat sont disponibles dans etcd-master à l'emplacement suivant.

``` bash
--cacert /etc/kubernetes/pki/etcd/ca.crt
--cert /etc/kubernetes/pki/etcd/server.crt
--key /etc/kubernetes/pki/etcd/server.key
```

``` bash
kubectl exec etcd-master -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl get / --prefix --keys-only --limit=10 --cacert /etc/kubernetes/pki/etcd/ca.crt --cert /etc/kubernetes/pki/etcd/server.crt  --key /etc/kubernetes/pki/etcd/server.key"
```

### kube-apiserver

1. authenticate user
2. validate request
3. retrieve data
4. update ETCD
5. Scheduler
6. kubelet

`ps -aux | grep kube-apiserver` pour avoir les informations

### kube-controller-manager

- watch status
- remediate situation

node monitor period: 5s
node monitor grace period: 40s
pod eviction timeout: 5m

`ps -aux | grep kube-controller-manager` pour avoir les informations

### kube-scheduler

il crée les pods par rapport au resource

1. filter nodes
2. rank nodes

`ps -aux | grep kube-scheduler` pour avoir les informations