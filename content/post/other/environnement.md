---
title: "Mon environnement"
description: "comment travailler proprement ?"
date: "2021-08-28"
author: "Franck Lafon"
license: "CC BY-NC-ND"
lastmod: "2022-07-07"
image: environnement.jpg
categories:
 - Other
---

## Visual studio code

- Tasks

- Les extensions

pour comparer 2 fichiers: palette fichier: comparer le fichier actif avec ...

## Mon terminal

shell -> zsh

<https://blog.zhaytam.com/2019/04/19/powerline-and-zshs-agnoster-theme-in-vs-code/>

## tmux

.tmux.conf

``` bash
set-option -g default-terminal "screen-256color"
set -g mouse on

# Add keybind for maximizing and minimizing tmux pane
bind -r m resize-pane -Z

# tmux plugin manager
set -g @plugin 'tmux-plugins/tpm'

# list of tmux plugins
set -g @plugin 'jimeh/tmux-themepack' # to configure tmux theme
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-resurrect' # persist tmux sessions after computer restart
set -g @plugin 'tmux-plugins/tmux-continuum' # automatically saves sessions for you every 15 minutes

set -g @themepack 'powerline/default/cyan' # use this theme for tmux

set -g @resurrect-capture-pane-contents 'on' # allow tmux-ressurect to capture pane contents
set -g @continuum-restore 'on' # enable tmux-continuum functionality

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

```

## speed test

<https://github.com/ddo/fast/releases>

`snap Install fast`

Ou si vous êtes sous Mac, vous pouvez aussi faire un petit :

`brew install fast`

## Avoir son ip publique

`curl ifconfig.me/ip`
