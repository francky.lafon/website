---
title: "BGP"
description: "La gestion du routage bgp"
date: "2020-06-15"
draft: true
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: bgp.jpg
categories:
 - Réseau
---