---
title: "Firewall - Configuration"
description: "Pense-bête pour la gestion des firewalls en CLI"
date: "2020-06-15"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: docker.jpeg
categories:
 - Réseau
---

### Mode supérieur / configuration / voir / redémarrer / arrêt / Backup

* enable / conf t / show / reload / shutdown / copy startup run start -> Cisco ASA
* -> Fortigate
* -> Stormshield

## Mise à jour (Update)

Fortigate
`execute restore image tftp <filename> <tftp_ipv4>`
`execute restore image tftp image.out 192.168.1.168`
`execute update-now`

### Update ASA

### Update Fortigate

`execute restore image tftp <filename> <tftp_ipv4> / execute restore image tftp image.out 192.168.1.168 / execute update-now`

### Update Stormshield

## Hostname

### Hostname ASA

`hostname x`

### Hostname Fortigate

   `config system global / set hostname xxxxx / end`

### Hostname Stormshield

## High Availability

### HA ASA

   `ip address active_addr netmask standby standby_addr`

   `failover lan unit primary / failover lan interface / failover interface ip if_name [ip_address mask standby interface phy_if / failover link if_name phy_if / failover`

### HA Fortigate

   `config system ha / set mode a-p / set group-name xxx / set password xxx / set hbdev port5 50 port6 50`

   `get system status`

### HA Stormshield

## NTP

### NTP Cisco

`ntp authenticate / ntp trusted-key key_id / ntp authentication-key / ntp server ip_address`

### NTP Fortinet

### NTP Stormshield

## Password

### Password Cisco

`passwd password / enable password`

### Password Fortinet

### Password Stormshield

## banner

banner ~

//###################################################################################################################################
//##                                   WARNING:                                                                                    ##
//## Notice that only capable Administrators should be allowed to connect to the system.                                           ##
//###################################################################################################################################

## interface

### Interface Cisco

`interface vlan x / nameif outside / security-level 0 / ip address x.x.x.x x.x.x.x / no shutdown`

### Interface Fortinet

### Interface Stormshield

## VLAN

### VLAN Cisco

   `interface vlan x / nameif inside / security-level 100 / ip address x.x.x.x x.x.x.x`

### VLAN Fortinet

### VLAN Stormshield

## Etherchannel

### Etherchannel Cisco

   `interface vlan x / nameif outside / security-level 0 / ip address x.x.x.x x.x.x.x / no shutdown`

### Etherchannel Fortinet

### Etherchannel Stormshield

## Serveur DHCP

dhcpd address ip_address-ip_address interface_name
dhcpd dns dns1
dhcpd lease lease_length
dhcpd domain domain_name
dhcpd ping_timeout
dhcpd enable

dhcprelay server ip_address
dhcprelay enable interface

## Route par défaut

ip default-gateway
ip default-network
ip route x.x.x.x x.x.x.x

## Serveur DNS

dns domain-lookup interface_name
dns server-group DefaultDNS
name-server ip_address

## Rules

## Access Rules

## NAT

## Accès SSH

> enable
configuration terminal

(config) domain-name toto.com
(config) crypto key generate rsa modulus 2048

(config) aaa authentication ssh console LOCAL

(config) ssh x.x.x.x x.x.x.x inside
(config) ssh timeout 5

(config) end

## Accès ADSM (only Cisco ASA)

(config) http server enable

(config) http x.x.x.x x.x.x.x inside

## network Object / Groups

conf t
object network VLAN_Number_Name
subnet X.X.X.X X.X.X.X

## Service Object / Groups

## Debug VPN

 ``` cli
show ipsec phase1 / phase 2
exec ping
diag vpn ike log-filter dst-addr
diag debug app ike -1
diag debug en
diag vpn ike gateway list name xxx
```
