---
title: "Switch - Configuration"
description: "Pense-bête pour la gestion des firewalls en CLI"
date: "2020-06-15"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: docker.jpeg
categories:
 - Réseau
---

## Mode supérieur / configuration / voir / redémarrer / arrêt / Backup

`enable / conf t / show / reload / shutdown / copy startup run start` -> cisco

`System-view/  / display / / save main / save backup` -> HP

## Mise à jour

Pour HPE

```shell
interface vlan 1
ip address 1.1.1.1 255.255.255.0
tftp 1.1.1.254 get xxxx.ipe
boot-loader file flash:/xxxx.ipe slot 1 main
display boot-loader
save
reboot
display version
```

## Hostname

`hostname x` -> Cisco
`config system global / set hostname xxxxx / end`

## Password

## Banner

system-view
header login

## Management

```shell
system-view
user-interface aux 0
user-interface vty 0 15
protocol inbound ssh
authentication-mode scheme

local-user x
password simple x
authorization-attribute user-role level-7
service-type terminal

display user-interface
```

## Stack (Only Cisco)

## Network_Time_Protocol

## IRF (Only HP)

* SW 1
`system-view / irf member 1 renumber 2 / reload / irf member 1 priority 10 / quit / save`

`display irf configuration / Reboot`

`Domaine irf 100 / irf domain 100 / quit`

`interface GigabitEthernet 1/0/1 / shutdown / quit`

`irf-port 1/1 / port group interface GigabitEthernet 1/0/1 / quit`

`interface GigabitEthernet 1/0/1 / undo shutdown / quit / save`

* SW 2
`system-view / irf domain 100`

`interface GigabitEthernet 2/0/1 / shutdown / quit`

`irf-port 2/2 / port group interface GigabitEthernet 2/0/1 / quit`

`interface GigabitEthernet 2/0/1 / undo shutdown / quit / save`

* Activer IRF
`irf-port-configuration active`

* Les SW redémarrent automatiquement
`display irf configuration / display irf topology`

## SW_L2

`system-view / interface GigabitEthernet1/0/5 / description x / port link-type trunk / port trunk permit vlan all / quit`

## SW_L3_Add_VLAN

`system-view / vlan x / name VLAN_NAME_x / quit`

## SW_L3_vlan_IP

`system-view / interface Vlan-interface xx / ip address X.X.X.X X.X.X.X / quit`

## SW_L3_ROUTES

`ip route-static X.X.X.X X.X.X.X X.X.X.X`
