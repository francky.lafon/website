---
title: "Secure Shell (SSH)"
description: "Comment voir le bout du tunnel ?"
date: "2021-08-28"
author: "Franck Lafon"
license: "CC BY-NC-ND"
lastmod: "2021-08-28"
draft: false
image: ssh.png
categories:
 - Système
---

Au début des années 90, les réseaux informatiques étaient en train de se développer à un rythme effréné et les administrateurs système avaient besoin d'un moyen sûr et sécurisé pour accéder à distance à leurs systèmes. C'est à ce moment que la technologie SSH (Secure Shell) est apparue pour remplir ce vide.

SSH a été développé en 1995 par Tatu Ylönen, un chercheur finlandais en sécurité informatique. Il a créé SSH pour remplacer les protocoles de connexion distante peu sécurisés, tels que Telnet et rlogin, qui transmettaient les informations en clair et étaient vulnérables aux attaques de l'Internet public.

Depuis son lancement, SSH est devenu l'outil de choix pour les administrateurs système et les développeurs qui souhaitent accéder à distance à leurs ordinateurs et serveurs. Il offre une connectivité fiable et sécurisée pour gérer les systèmes distants, ce qui le rend indispensable pour les entreprises et les organisations.

Avec la technologie de cryptage de bout en bout et l'authentification à clé publique, SSH garantit la sécurité des communications et protège les informations sensibles des regards indiscrets. De plus, il est simple à utiliser et ne nécessite qu'un client SSH installé sur l'ordinateur local et un serveur SSH en cours d'exécution sur l'ordinateur distant.

Les fichiers de configuration sont stocké dans `/etc/ssh/`

## ssh

``` shell
Host * # range d'ip
    Port 22 # port d'écoute
    PasswordAuthentication no # à voir
    StrictHostKeyChecking ask # option à voir
    IdentityFile ~/.ssh/id_ed25519
    KexAlgorithms curve25519-sha256@libssh.org # the key exchange methods that are used to generate per-connection keys
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com # the ciphers to encrypt the connection
    MACs    hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com #  the message authentication codes used to detect traffic modification
Include /etc/ssh/ssh_config.d/*.cosys
```

## sshd

"sshd" est l'acronyme pour "Secure Shell Daemon" et c'est le processus qui exécute le serveur SSH sur un ordinateur. Lorsqu'un client SSH se connecte à un serveur distant, il établit une connexion avec le processus "sshd" sur le serveur. Le serveur SSH utilise le protocole SSH pour authentifier le client et établir une session sécurisée pour les communications. En gros, le processus "sshd" sur le serveur est la partie serveur de la solution SSH.

```shell
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
KexAlgorithms curve25519-sha256@libssh.org # the key exchange methods that are used to generate per-connection keys
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com # the ciphers to encrypt the connection
MACs    hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com # L'algorithme MAC est utilisé dans la version 2 du protocole pour la protection de l'intégrité des données

# Authentication
LoginGraceTime 2m # Le serveur se déconnecte après ce délai si l'utilisateur ne s'est pas connecté
PermitRootLogin no # login avec le compte root
StrictModes yes # Spécifie si sshd doit vérifier les modes et le propriétaire des fichiers de l'utilisateur et du répertoire de base (home directory) de l'utilisateur avant d'accepter une connexion
MaxAuthTries 3
MaxSessions 3
AuthenticationMethods publickey
PubkeyAuthentication yes
PasswordAuthentication no
AuthorizedKeysFile      .ssh/authorized_keys


# Options
X11Forwarding yes
ChallengeResponseAuthentication no
PrintMotd no

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

Subsystem       sftp    /usr/libexec/openssh/sftp-server
```

## la gestion des clés

il y a 2 endroits ou sont stocké les clés:

- dans le home de l'utilisateur.

- les clés de la machine qui sont dans le /etc/ssh

pour qu'une machine soient autorisé il faut que le clé public soit dans le fichier `~/.ssh/authorized_keys`
