---
title: "Linux"
description: "Comment devenir un barbue ?"
date: "2022-07-06"
author: "Franck Lafon"
license: "CC BY-NC-ND"
lastmod: "2022-07-06"
image: linux.jpg
categories:
 - Système
---


## les folders principaux

/bin → binary (executable des commandes essenciel)

/boot → bootstrap (fichier statique chargeur d’amorçage)

/etc → editing text config (fichier de configuration)

/dev → device

/lost and found → dossier de la commande fsck

/opt → application hors gestionnaire de paquet

       /mnt → mount

/run → runtime system (info relative au system depuis son dernier reboot)

/sbin → super binary (exe system essentiels)

/srv → service (data pour les services du system)

/usr → Unix system ressources

/var → variable

## en vrac

telnet → pour le troubleshooting network

neofetch → paquet pour avoir les infos du poste

init → 1er process sur les distributions ubuntu

dmesg → log du boot

umask → droits chown - umask (exemple: 777 - 222 = 444)

commande whereis est différente de locate

## Shabang

`-xe`: mode debug

## Hostnamectl

`hostnamectl status`

## Cut

`cut -d: -f1`

-d delimiter

-f fields

## Grep (Globale/REgular/Expression/Print)

`grep -E`: egrep

`grep -F`: fgrep

`grep -r`: rgrep

`grep toto /tmp/*`

-r récursif

-i  no key sensitive

## Sed (Stream EDitor)

SED command in UNIX stands for stream editor and it can perform lots of functions on file like searching, find and replace, insertion or deletion.

`sed -i.bck 's/unix/linux/' toto.txt` : Replacing or substituting string

`sed -i.bck 's/unix/linux/2' toto.txt` : Replacing the nth occurrence of a pattern in a line

`sed -i.bck 's/unix/linux/g' toto.txt` : Replacing all the occurrence of the pattern in a line

`sed -i.bck '3 s/unix/linux/' toto.txt` : Replacing string on a specific line number

`sed -i.bck '1,3 s/unix/linux/' toto.txt` : Replacing string on a range of lines

`sed -i.bck -n 's/unix/linux/p' toto.txt` : Printing only the replaced lines

`sed -i.bck '/tata/d' toto.txt`: delete line with the occurrence

`sed '5d' filename.txt`: delete the nth line

`sed '$d' filename.txt`: delete the last line

`sed '3,6d' filename.txt` delete between the 3th to 6th line

## Rsync

`rsync -av toto root@machine:/tmp`

-a archive

-v verbose

## ln

`ln -s /pt d’arriver /pt départ`

## awk

## Troubleshoot linux

`free -h`

`du -sh`

`cat /var/log/messages`

`cat /var/log/secure`

`htop -u toto`: Cette commande permet d'afficher les processus d'un utilisateur.

`htop -t`: tree
