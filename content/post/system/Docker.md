---
title: "Docker"
description: "docker"
date: "2022-04-11"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: docker.jpeg
categories:
 - Système
---

Hello,

voici mes tips pour faire du docker propre. Dans un premier temps nous allons voir comment l'installer.  voir mon repository Ansible.

`ansible-playbook 02-deploy-docker.yml -K`

## Les commandes docker

`docker ps`

`docker search/pull`

`docker container/volume/image/network ls`

`docker run`

`docker start/stop`

`docker exec`

PS: a voir les autres commandes …

[https://docs.docker.com/engine/reference/commandline/docker/](https://docs.docker.com/engine/reference/commandline/docker/)

## Les commandes docker-compose

`docker-compose up/down`

`docker-compose ps`

`docker-compose pull`

`docker-compose logs`

## Fichier docker-compose

``` yaml
version: '3.8'
services:
  toto:
    image: image:version

    container_name: name

    environment:
      - PUID=0
      - PGID=0

    volumes:
      - host:container

    labels:
      - "traefik.enable=true"

    networks:
      - internal

    ports:
      - 7878:7878

    restart: unless-stopped
```

## Trivy

## Cloisonnement container

Voici les recommandations de l’ANSSI sur le sujet.

<[https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-au-deploiement-de-conteneurs-docker/](https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-au-deploiement-de-conteneurs-docker/)>

## Sécurité

Pour faire une évaluation de la sécurité sur docker.

```bash
docker run -it --net host --pid host --userns host --cap-add audit_control \
    -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
    -v /var/lib:/var/lib \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /usr/lib/systemd:/usr/lib/systemd \
    -v /etc:/etc --label docker_bench_security \
    docker/docker-bench-security
```

## Deep docker

## Stop using Alpine Docker images

## What is a distroless image?

_D'abord,_ I have to say **this isn't something new**, and I mean it. It has been around for years, and you can check it in [GoogleContainerTools/distroless](https://github.com/GoogleContainerTools/distroless).

> “Distroless” images contain only your application and its runtime dependencies. They do not contain package managers, shells or any other programs you would expect to find in a standard Linux distribution

<https://hub.docker.com/_/scratch>

``` dockerfile
FROM golang:1.17-bullseye as build

WORKDIR /go/src/app
ADD . /go/src/app

RUN go get -d -v ./...
RUN go build -o /go/bin/app

##

FROM gcr.io/distroless/base-debian11

COPY --from=build /go/bin/app /
CMD ["/app"]
```
