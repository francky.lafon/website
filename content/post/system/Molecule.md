---
title: "Molecule"
description: "Molecule"
date: "2023-02-12"
author: "Franck Lafon"
license: "CC BY-NC-ND"
draft: false
image: molecule.png
categories:
 - Système
 - devops
---

## Introduction

Molecule est un outil open source pour tester des configurations Ansible. Il aide les développeurs à tester de manière efficace leur code Ansible en fournissant une infrastructure pour exécuter des scénarios de tests dans un environnement contrôlé. Molecule peut exécuter des tests sur des plateformes virtuelles, telles que VirtualBox et Docker, ainsi que sur des plates-formes cloud, telles que Amazon Web Services (AWS) et Google Cloud Platform (GCP).

Avec Molecule, les développeurs peuvent définir des scénarios de tests dans des fichiers de spécification, tels que des scénarios de test de configuration de base, de déploiement, de sécurité et de conformité. Molecule peut également vérifier la validité du code Ansible en utilisant des outils tels que YAML Lint et Ansible Lint.

L'utilisation de Molecule peut améliorer la qualité et la stabilité du code Ansible en permettant aux développeurs de tester leur code de manière plus automatisée et en fournissant une infrastructure pour exécuter des tests dans un environnement contrôlé. En outre, Molecule peut également améliorer la collaboration et la collaboration entre les équipes de développement en permettant de partager et de maintenir facilement des tests de configurations.

## Setup de molecule

```bash
sudo dnf install -y gcc python3-pip python3-devel openssl-devel python3-libselinux
python3 -m pip install molecule ansible-core
```

## Dossier d'un scénario

```bash
Molecule
│   ├── default
│   │   ├── converge.yml
│   │   ├── INSTALL.rst
│   │   ├── molecule.yml
│   │   ├── testinfra.py
│   │   ├── scenario.yml
│   │   ├── .kitchen.yml

```

Fichier converge : Ce fichier définit le code Ansible qui sera utilisé pour configurer et déployer les ressources dans l'environnement de test. Il peut inclure des tâches Ansible, des modules Ansible, des playbooks Ansible, etc.

Fichier install.rst : Ce fichier définit les dépendances et les prérequis pour exécuter les tests dans Molecule. Il peut inclure des instructions pour installer des packages, des modules et d'autres éléments requis pour exécuter les tests.
``` yaml
dependency:
  name: galaxy
driver:
  name: docker
platforms:
- name: default
  image: rockylinux:8.6.20227707

Fichier molecule.yml : Ce fichier définit les paramètres de configuration pour les scénarios de tests dans Molecule. Il peut inclure des paramètres pour spécifier le système d'exploitation cible, la plate-forme virtuelle ou cloud utilisée, la stratégie de convergence, la stratégie de vérification et les paramètres de test supplémentaires.

Fichier testinfra.py : Ce fichier définit des tests de vérification qui peuvent être utilisés pour tester la configuration du système dans l'environnement de test. Il peut inclure des tests pour vérifier l'état des services, des fichiers, des processus, etc.
provisioner:
    name: ansible
    inventory:
        links:
            hosts: /etc/ansible/hosts
    options:
    vvv: True
```

Fichier scenario.yml : Ce fichier définit les paramètres de scénario pour un scénario de test spécifique dans Molecule. Il peut inclure des paramètres pour spécifier le système d'exploitation cible, la plate-forme virtuelle ou cloud utilisée, la stratégie de convergence, la stratégie de vérification, etc.

Fichier .kitchen.yml : Ce fichier est utilisé pour définir les paramètres de configuration pour les tests avec Kitchen-Ansible, qui est une autre infrastructure de test pour les configurations Ansible.

## Les séquences

- create

- check

- converge

- destroy

- test

## Les commandes

`molecule init` : Cette commande initialise un nouveau projet Molecule et crée les dossiers et fichiers nécessaires pour commencer à écrire et exécuter des scénarios de tests.

`molecule create` : Cette commande crée des instances virtuelles ou des machines virtuelles pour exécuter les scénarios de tests dans.

`molecule converge` : Cette commande exécute le code Ansible défini dans le fichier converge pour configurer et déployer les ressources dans l'environnement de test.
- lint
- dependency
- cleanup
- destroy
- syntax
- create
- prepare
- converge
- idempotence
- side_effect
- verify
- cleanup
- destroy

verifier:
    name: testinfra

`molecule verify` : Cette commande exécute les tests de vérification définis dans le fichier testinfra.py pour tester la configuration du système dans l'environnement de test.

`molecule destroy` : Cette commande détruit les instances virtuelles ou les machines virtuelles créées pour les scénarios de tests.

`molecule test` : Cette commande exécute les étapes de création, convergence, vérification et destruction dans un seul cycle, ce qui est utile pour effectuer un test complet.

`molecule list` : Cette commande liste les différents scénarios de tests disponibles dans le projet.

`molecule login` : Cette commande permet de se connecter à une instance virtuelle ou une machine virtuelle pour inspecter les ressources déployées dans l'environnement de test.

il y a une option pour sélectionner un scénario `-s --scenario-name`