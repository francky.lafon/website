---
title: "S3curl"
description: ""
date: "2023-02-17"
author: "Franck Lafon"
license: "CC BY-NC-ND"
draft: false
image: ssh.png
categories:
 - Système
---

Test avec le client s3curl: <https://github.com/rtdp/s3curl/blob/master/README>

## Configuration du client s3curl

1) Faire un fichier de configuration dans le home avec comme nom .s3curl
2) ./s3curl.pl --id=[friendly-name] -- <http://s3.amazonaws.com> -namespace-bucket-name
