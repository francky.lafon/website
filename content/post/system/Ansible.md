---
title: "Ansible"
description: "Ansible"
date: "2021-12-26"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: "ansible.jpg"
categories:
 - Système
---
## Introduction

C'est quoi Ansible. En quelques mots, C'est une solution qui permet de faire un ensemble de tâche de façon automatisé sur plusieurs machines en même temps.

## Les fichiers de configuration

Ce qui faut retenir: il y a plusieurs endroit ou mettre le fichier de configuration.

`/etc/ansible/ansible.cfg`

`~/.ansible.cfg`

`./ansible.cfg`

variable `ANSIBLE_CONFIG=`

`/etc/ansible/ansible.cfg` : Ce fichier permet de configurer ansible.

`/etc/ansible/hosts`: permet de déclarer les hosts.

## Les playbooks

Les playbooks sont les fichiers qui décrive les différentes tasks. Voici la structure minimum

``` yaml
---
- hosts:
  tasks:
  - name:
    dnf: ...
```

## Les rôles

## Astuce pour le troubleshooting

pour le troubleshoot d'un playbook:
`--check`
`--step` -> faire task by task

Pour faire plusieurs tasks avec une condition (par exemple un os en particulier)

``` yaml
- tasks:
  - name: block
    block:
      - name: blabla
       ...
    when:
```

## Ansible Architecture and Design

le fichier d'inventory peut être en INI, yaml ou json

on peux mettre des variables pour un groupe de machine dans le fichier d'inventaire.

on peux mettre dans le fichier de configuration ou playbook l'option host_key_checking = False qui permet de faire l'action même si les clés ssh ne sont pas passées.

`ansible all -m ping -o` -> fait un test de connection sur toutes les machines et le résultat est sur une ligne

`ansible CentOS --list-hosts` -> liste les machines d'un group

`ansible-inventory --graph` -> même résultat que la commande du dessus

`ansible ~.*3 --list-hosts` -> ~=regex .=any caracters=any number of previous 3=ending with the number 3

`ansible all -m command -a 'id' -o` -> fait la commande id sur toute les machines

ansible CentOS1 -m setup -> pour avoir les infos de la machine

## Ansible playbooks, Creating and executing

On peux faire un dossier groups_vars et hosts_vars pour les variables lier à un groupe | machine.

Remarque pour DA: ... pourquoi ne pas l'avoir fait plus tôt

Pour des fichiers de configuration faire un dossier Template et mettre les fichiers en format .j2

Le dossier vars pour les variable commune.

Remarque pour DA: ... pourquoi ne pas l'avoir fait plus tôt

Les Facts, Cela permet d'avoir des informations de la machines ou le playbook tourne et les mettre en variables.
voir le dossier dan s

Remarque pour DA: on peux faire des facts personnalisé ... NP | CD

## Ansible playbook, deep dive

voir les loops ...
il y a beaucoup de with_
with_togethers
with_subelements
with_file
with_sequence
with_dict

Remarque DA: actuellement  mauvais choix sur la gestion des secrets quand on aura plusieurs fichier vault ...

## Creating Module and plugins

on peux faire des module avec des script bash ... après c'est mieux en python

## Formation into deep dive Ansible

## Section 1/2

Introduction et installation du lab avec docker

faire un clone sur la machine hôte:

<https://github.com/spurin/diveintoansible-lab>

faire dans le container ubuntu-c

<https://github.com/spurin/diveintoansible>
