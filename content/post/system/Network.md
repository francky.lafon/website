---
title: "NetworkManager"
description: "gestion du réseau sur une machine Linux"
date: "2022-06-07"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: network.jpg
categories:
 - Système
 - Réseau
---



## Troubleshooting Network

layer 1: Physical

- `nmcli`

- `ethtool eth0`

- `ip link show`

- `ip -br link show`

- `ip -s link show eth0`

layer 2: Data link

- `ip neighbor show`

layer 3: the network/internet

- `ip -br address show`

- `ping -c1 8.8.8.8`

- `mtr / traceroute / tracepath`

- `ip route show 10.0.0.0/8`

- `nslookup / dig`

layer 4: Transport

- `ss -tulnp4 / netstat -tulpn`

- `telnet toto 3306 / nc toto -u 80`

- `tcpdump -i eth0`

## Configuration des cartes réseaux
