---
title: "Firewalld"
description: "Pense-bête pour le service linux firewalld"
date: "2021-12-27"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: firewall.jpg
categories:
 - Système
---

Hello,

le service firewalld est sur les distributions de la famille Redhat. Il est au dessus de iptables. Il y a plusieurs choses à voir: les zones / service et ports

## Les zones

- drop: le niveau de confiance le plus bas. Toutes les connexions entrantes sont interrompues sans réponse et seules les connexions sortantes sont possibles.

- block: similaire à ce qui précède, mais au lieu de simplement interrompre les connexions, les demandes entrantes sont rejetées avec un message icmp-host-prohibited ou icmp6-adm-prohibited.

- public: représente les réseaux publics, non fiables. Vous ne faites pas confiance aux autres ordinateurs, mais vous pouvez autoriser certaines connexions entrantes au cas par cas.

- external: les réseaux externes dans l’éventualité où vous utilisez le pare-feu comme passerelle. Il est configuré pour le masquage NAT de sorte que votre réseau interne reste privé mais accessible.

- interne: l’autre côté de la zone externe, utilisé pour la partie interne d’une passerelle. Les ordinateurs sont assez fiables et certains services supplémentaires sont disponibles.

- dmz: utilisé pour les ordinateurs situés dans un DMZ (ordinateurs isolés qui n’auront pas accès au reste de votre réseau). Seules certaines connexions entrantes sont autorisées.

- work: utilisé pour les machines de travail. Fait confiance à la plupart des ordinateurs du réseau. Quelques services supplémentaires pourraient être autorisés.

- home: un environnement domestique. Cela implique généralement que vous faites confiance à la plupart des autres ordinateurs et que quelques services supplémentaires seront acceptés.

- trusted: fais confiance à toutes les machines du réseau. La plus ouverte des options disponibles et doit être utilisée avec parcimonie.

## Les commandes

`firewall-cmd --state`

`firewall-cmd --get-default-zone`

`firewall-cmd --get-active-zones`

`firewall-cmd --list-all`

`firewall-cmd --check-config`

`firewall-cmd --zone=internal --change-interface=eth0`

`firewall-cmd --zone=internal --remove-service=`

`firewall-cmd --zone=internal --add-service=`

`firewall-cmd --zone=internal --add-port=`

PS: penser à mettre le `--permanent` et faire un `firewall-cmd --reload`

## Créé un service

Voici un exemple pour créé un service custom

/usr/lib/firewalld/services/example.xml

``` xml
<?xml version="1.0" encoding="utf-8"?>

<service>
  <short>example</short>
  <description>example</description>
  <port protocol="tcp" port="22"/>
  <port protocol="udp" port="8888"/>
</service>
```

PS: faire un `firewall-cmd --reload`  pour la prise en compte.

## ICMP

Gestion de ICMP

`firewall-cmd --zone=public --remove-icmp-block-inversion --permanent`

`firewall-cmd --zone=public --add-icmp-block-inversion --permanent`

`firewall-cmd --zone=public --query-icmp-block-inversion --permanent`

Gestion des options de ICMP

`firewall-cmd --query-icmp-block=echo-request`

`firewall-cmd --add-icmp-block=echo-request`

`firewall-cmd --remove-icmp-block=echo-request`

les options de ICMP

- address-unreachable
- bad-header
- beyond-scope
- communication-prohibited
- destination-unreachable
- echo-reply
- echo-request
- failed-policy
- fragmentation-needed
- host-precedence-violation
- host-prohibited
- host-redirect
- host-unknown
- host-unreachable
- ip-header-bad
- neighbour-advertisement
- neighbour-solicitation
- network-prohibited
- network-redirect
- network-unknown
- network-unreachable
- no-route
- packet-too-big
- parameter-problem
- port-unreachable
- precedence-cutoff
- protocol-unreachable
- redirect
- reject-route
- required-option-missing
- router-advertisement
- router-solicitation
- source-quench
- source-route-failed
- time-exceeded
- time-reply
- timestamp-request
- tos-host-redirect
- tos-host-unreachable
- tos-network-redirect
- tos-network-unreachable
- ttl-zero-during-reassembly
- ttl-zero-during-transit
- unknown-header-type
- unknown-option

## Référence

[https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-8-fr](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-8-fr)
