---
title: "L'interface graphique sur Linux"
description: "Comprendre comment fonctionne l'interface graphique"
date: "2022-01-30"
author: "Franck Lafon"
license: "CC BY-NC-ND"
lastmod: "2022-01-30"
image: "xfce.jpg"
draft: false
categories:
 - Système
---


## Commande xfce

xfce4-screensaver-preferences

les 2 commandes si dessous sont que pour un user et non persistant au redémarrage.

> description de xset: This program is used to set various user preference options of the display.

```bash
xset s off
xset s noblank
```
