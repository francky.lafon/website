---
title: "Certificat"
description: "Gestion des certificats"
date: "2021-12-27"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: certificate.jpg
categories:
 - Sécurité
---

## Introduction sur les certificats

Les certificats sont des fichiers électroniques qui permettent de garantir l'identité d'un site web ou d'un service en ligne. Ils sont utilisés pour crypter les communications entre le navigateur de l'utilisateur et le site web, ce qui assure la confidentialité et l'intégrité des données transmises. Les certificats jouent également un rôle important dans la validation de l'identité d'un site web, ce qui aide les utilisateurs à vérifier que le site qu'ils visitent est le site authentique qu'ils cherchent à accéder.

L'histoire des certificats remonte à l'arrivée du commerce électronique au milieu des années 90. Au fur et à mesure que les transactions en ligne se sont multipliées, il est devenu clair que les utilisateurs avaient besoin d'une méthode sûre pour vérifier l'identité des sites web qu'ils visitaient. C'est à ce moment que les certificats numériques sont apparus pour remplir ce vide.

Il existe deux types de certificats : les certificats auto-signés et les certificats signés. Les certificats auto-signés sont générés par l'entité qui possède le site web ou le service en ligne et ne sont pas validés par une autorité de certification (CA) tierce. Les certificats signés, quant à eux, sont générés par une entité et validés par une CA tierce qui garantit l'identité de l'entité. Les certificats signés sont considérés comme plus fiables et plus sûrs que les certificats auto-signés, car ils sont soumis à une vérification rigoureuse de leur identité avant d'être émis.

Pour générer un certificat, vous devez d'abord créer une demande de certificat (CSR). La demande de certificat contient des informations sur l'entité qui demande le certificat, telles que le nom de domaine, l'adresse, etc. Une fois la demande de certificat envoyée à une CA, elle sera vérifiée et si toutes les informations sont correctes, un certificat sera émis et envoyé à l'entité.

En conclusion, les certificats sont un élément crucial pour la sécurité en ligne. Ils aident à protéger les communications et les transactions en ligne en cryptant les données et en validant l'identité des sites web. Les certificats signés sont considérés comme plus fiables que les certificats auto-signés, mais la génération d'un certificat requiert une demande de certificat qui doit être vérifiée par une CA tierce.

## Openssl

OpenSSL est un logiciel open source qui implémente les protocoles de sécurité Internet, tels que SSL (Secure Socket Layer) et TLS (Transport Layer Security). Il est largement utilisé pour fournir une sécurité renforcée à des applications web, des services de messagerie, des VPN, etc. OpenSSL permet de crypter les données transmises sur un réseau, de générer des certificats SSL/TLS et de vérifier la sécurité des connexions à distance.

OpenSSL est compatible avec un grand nombre de systèmes d'exploitation, notamment Linux, Unix, macOS et Windows, et il est souvent utilisé sur des systèmes serveur pour fournir une sécurité renforcée aux applications et services en ligne. Il est également utilisé pour la création de certificats SSL pour les sites web, ce qui permet de garantir la confidentialité des informations transmises entre le navigateur de l'utilisateur et le site web.

OpenSSL comprend également une collection d'outils en ligne de commande qui permettent aux administrateurs système et aux développeurs de gérer et de vérifier la sécurité des connexions réseau, de générer et de gérer des certificats, de chiffrer et de déchiffrer les fichiers, etc. Les outils d'OpenSSL sont souvent utilisés pour déboguer les problèmes de sécurité et pour gérer les configurations de sécurité.

En conclusion, OpenSSL est un logiciel de sécurité Internet open source largement utilisé pour garantir la sécurité des communications sur les réseaux. Il implémente les protocoles SSL/TLS et fournit une collection d'outils pour la gestion de la sécurité des connexions et des certificats. OpenSSL est compatible avec un grand nombre de systèmes d'exploitation et est souvent utilisé sur des systèmes serveur pour fournir une sécurité renforcée aux applications et services en ligne.

## Création d’un Certificat auto-signé

Mettre la `ca.key`  dans le `/etc/pki/tls/private`  ou `/etc/ssl/private`

Mettre les certificats dans  `/etc/pki/tls`  ou  `/etc/ssl/certs/`

`openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout .key -out .crt`

PS: Pour la demande de création du certificat il faut au minimum le subject CommonName (FQDN) `-subj ‘/CN=…’`

Attention à revoir: ressource et différence entre /etc/ssl et /etc/pki

## Mise en place d’un certificat

`cp xxx.crt /etc/pki/ca-trust/source/Anchor/`

`chmod 444 /etc/pki/ca-trust/source/Anchor/xxx.crt`

`update-ca-trust && reboot`

## Let’s encrypt

To be write !

## Ressource

[https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-a-certificate-authority-ca-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-a-certificate-authority-ca-on-ubuntu-20-04-fr)
