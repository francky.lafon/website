---
title: "Packer"
description: "Packer"
date: "2023-02-12"
author: "Franck Lafon"
license: "CC BY-NC-ND"
draft: false
image: packer.png
categories:
 - devops
---
## Introduction

Packer est un outil open source développé par Hashicorp qui permet de créer des images de machines virtuelles et de conteneurs de manière automatisée. Il peut être utilisé pour générer des images pour diverses plateformes telles que VirtualBox, VMware, AWS, Google Cloud, entre autres.

Packer fonctionne en exécutant des scripts de construction (appelés « builders ») qui décrivent comment une image doit être créée et configurée. Les builders peuvent installer des logiciels, configurer des paramètres système, exécuter des scripts, etc. Une fois que le builder a fini de configurer l'image, Packer la valide pour s'assurer qu'elle fonctionne correctement et la sauvegarde en tant qu'image.

Les avantages de l'utilisation de Packer incluent une automatisation accrue de la création d'images, une réduction des erreurs et des incohérences dans les images, ainsi qu'une réduction du temps nécessaire pour créer des images. Les images générées par Packer peuvent être utilisées pour déployer des machines virtuelles ou des conteneurs pour divers scénarios, tels que les tests, les développements, les déploiements de production, etc.

## Setup de Packer

```bash
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
sudo yum -y install packer
```

## La gestion de vm avec VMWare

``` hcl2
packer {
  required_plugins {
    vmware = {
      version = ">= 1.0.7"
      source  = "github.com/hashicorp/vmware"
    }
  }
}

source "vmware-vmx" "vm" {
  source_path      = "~/vmware/.vmx"
  ssh_username     = "vagrant"
  ssh_password     = "vagrant"
  shutdown_command = "sudo /usr/sbin/poweroff -f"
}

build {
  name    = "vm"
  sources = ["sources.vmware-vmx.vm"]
  provisioner "shell" {
    inline = ["echo toto"]
  }
  post-processor "vagrant" {}
  post-processor "compress" {}
}
```
