---
title: "Network Time Protocol (NTP)"
description: "ntp"
date: "2021-12-27"
draft: true
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: ntp.jpg
categories:
 - Système
---


## GNSS

Un **système de positionnement par satellites**[Note 1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-1) également désigné sous le sigle **GNSS** (pour _Géolocalisation et Navigation par un Système de Satellites_[1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-2),[Note 2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-3)) est un ensemble de composants reposant sur une [constellation](https://fr.wikipedia.org/wiki/Constellation_de_satellites "Constellation de satellites") de [satellites artificiels](https://fr.wikipedia.org/wiki/Satellite_artificiel "Satellite artificiel") permettant de fournir à un utilisateur par l’intermédiaire d'un récepteur portable de petite taille sa position [3D](https://fr.wikipedia.org/wiki/Trois_dimensions "Trois dimensions"), sa vitesse 3D et l'heure. Cette catégorie de système de [géopositionnement](https://fr.wikipedia.org/wiki/G%C3%A9opositionnement "Géopositionnement") se caractérise par une précision métrique, sa couverture mondiale et la compacité des terminaux, mais également par sa sensibilité aux obstacles présents entre le terminal récepteur et les satellites. Certains systèmes d'augmentation et de fiabilisation de portée régionale ou mondiales, gratuits ou payants, permettent de fiabiliser le système et d'améliorer les performances ([DGPS](https://fr.wikipedia.org/wiki/GPS_diff%C3%A9rentiel "GPS différentiel"), [EGNOS](https://fr.wikipedia.org/wiki/European_Geostationary_Navigation_Overlay_Service "European Geostationary Navigation Overlay Service"), _[Assisted GPS](https://fr.wikipedia.org/wiki/Assisted_GPS "Assisted GPS")_ (A-GNSS), etc.).

### GPS

Le système [GPS](https://fr.wikipedia.org/wiki/Global_Positioning_System "Global Positioning System"), se développe, à partir de 1978 (année de mise en service du premier satellite) et devient disponible librement en 1994 (avec un accès qui n'est alors plus réservé à l'[armée américaine](https://fr.wikipedia.org/wiki/Forces_arm%C3%A9es_des_%C3%89tats-Unis "Forces armées des États-Unis")) et pleinement opérationnel en 1995 (avec une constellation de 24 satellites). Il est alors, pendant un an, le seul système de positionnement par satellite, pleinement efficace et fonctionnel.

Un an plus tard (1996), le système GLONASS russe devient, lui aussi, pleinement opérationnel. Cependant, entre 1999 et 2010 (à cause de l’obsolescence de GLONASS), le système [GPS](https://fr.wikipedia.org/wiki/Global_Positioning_System "Global Positioning System") était redevenu le seul système mondial de navigation satellitaire entièrement opérationnel. En 2015, Il est constitué de 31 satellites[7](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-gpsstatut-9) (24 à l'origine) en orbite intermédiaire (MEO) en six plans orbitaux. Le nombre exact de satellites varie en fonction des remplacements de satellites en fin de vie.

### GLONASS

Le système [GLONASS](https://fr.wikipedia.org/wiki/GLONASS "GLONASS") de l’ex Union Soviétique, aujourd’hui la [Russie](https://fr.wikipedia.org/wiki/Russie "Russie") (en russe _Global'naya Navigatsionnaya Sputnikovaya Sistema_), était également une constellation fonctionnelle apparue en 1995 et rendue opérationnelle dès 1996, mais avec l’écroulement de l’Union soviétique, il n’était plus entretenu, provoquant des pannes matérielles dès 1997 (deux ans après son lancement), s'aggravant entre 1997 et 2000 et générant des trous de couvertures, rendant obsolète et non-fonctionnel ce système de positionnement. Entre 2000 et 2010, la disponibilité était donc devenue partielle. En 2005, cependant, la Fédération de Russie s’est engagée à le restaurer avant 2010, avec une collaboration indienne dans ce projet. Entre 2008 et 2010, de nouveaux satellites sont lancés, le rendant de nouveau, progressivement fonctionnel. Depuis 2010, il est enfin redevenu opérationnel et depuis 2011, sa précision s'améliore, le rendant pleinement efficace. Entre [octobre](https://fr.wikipedia.org/wiki/Octobre_2011 "Octobre 2011") et [décembre](https://fr.wikipedia.org/wiki/D%C3%A9cembre_2011 "Décembre 2011") [2011](https://fr.wikipedia.org/wiki/2011 "2011"), pour la première fois, la constellation GLONASS couvre 100 % de la surface de la planète. L'[iPhone](https://fr.wikipedia.org/wiki/IPhone "IPhone") 4S et le [Samsung Wave III](https://fr.wikipedia.org/w/index.php?title=Samsung_Wave_3&action=edit&redlink=1 "Samsung Wave 3 (page inexistante)") deviennent en 2011, les premiers smartphones grand public (en dehors du marché russe) à recevoir nativement les signaux GLONASS et à les utiliser pour évaluer le positionnement[8](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-10),[9](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-11).

### Galileo

L’Union européenne a signé avec l’[agence spatiale européenne](https://fr.wikipedia.org/wiki/Agence_spatiale_europ%C3%A9enne "Agence spatiale européenne") en [mars](https://fr.wikipedia.org/wiki/Mars_2002 "Mars 2002") [2002](https://fr.wikipedia.org/wiki/2002 "2002") l’accord sur le développement du système global [Galileo](https://fr.wikipedia.org/wiki/Galileo_(syst%C3%A8me_de_positionnement) "Galileo (système de positionnement)"). Le coût est estimé à environ 3 milliards d’euros[10](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-12). La constellation finale sera constituée de 24 satellites qui devraient être opérationnels en 2017, ainsi que de 6 satellites de secours[11](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-13). Le premier satellite expérimental a été lancé le [28](https://fr.wikipedia.org/wiki/28_d%C3%A9cembre "28 décembre") [décembre](https://fr.wikipedia.org/wiki/D%C3%A9cembre_2005 "Décembre 2005") [2005](https://fr.wikipedia.org/wiki/2005 "2005"). Un second satellite de validation a été lancé en 2008. Le [11](https://fr.wikipedia.org/wiki/11_septembre "11 septembre") [septembre](https://fr.wikipedia.org/wiki/Septembre_2015 "Septembre 2015") [2015](https://fr.wikipedia.org/wiki/2015 "2015"), dix satellites étaient d'ores et déjà en orbite[12](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-14), et on en comptait huit de plus fin 2016[13](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-15). Les premiers services sont opérationnels à partir du [15](https://fr.wikipedia.org/wiki/15_d%C3%A9cembre "15 décembre") [décembre](https://fr.wikipedia.org/wiki/D%C3%A9cembre_2016 "Décembre 2016") [2016]

Les signaux de navigation de Galileo sont compatibles avec ceux du GPS, permettant aux récepteurs de les combiner pour augmenter la précision ainsi que la véracité du point.

### Compass

La [Chine](https://fr.wikipedia.org/wiki/Chine "Chine") a commencé à transformer son système régional [Beidou](https://fr.wikipedia.org/wiki/Beidou "Beidou") en système global. Ce programme est appelé « _Compass_ » par l’agence d’informations chinoises officielle [Xinhua News Agency](https://fr.wikipedia.org/wiki/Xinhua "Xinhua").

Le système Compass doit comporter trente satellites en orbite MEO et cinq géostationnaires. Cette annonce est accompagnée d’une invitation à d’autres pays désirant y collaborer, alors que la Chine est également engagée dans le programme Galileo.

### IRNSS

Le système [IRNSS](https://fr.wikipedia.org/wiki/Indian_Regional_Navigational_Satellite_System "Indian Regional Navigational Satellite System") (_Indian Regional Navigational Satellite System_) est un projet de système autonome de navigation régionale construit et contrôlé par le gouvernement indien. Il doit permettre une précision absolue de 20 mètres sur l’Inde et s'étendrait jusqu’à 1 500 à 2 000 km autour de son voisinage. Le but est d'avoir un système entièrement sous contrôle indien, le segment spatial, terrestre et les récepteurs étant développés par l’Inde.

Le projet a été approuvé par le gouvernement indien en [mai](https://fr.wikipedia.org/wiki/Mai_2006 "Mai 2006") [2006](https://fr.wikipedia.org/wiki/2006 "2006"), avec un objectif de développement en six à sept ans.

### QZSS

Le système [QZSS](https://fr.wikipedia.org/wiki/Quasi-Zenith_Satellite_System "Quasi-Zenith Satellite System") (_Quasi-Zenith Satellite System_), est développé par le Japon pour un premier lancement en 2008. Il est constitué de trois satellites géostationnaires permettant le transfert de temps et une augmentation du GPS. Il couvrira le Japon et sa région[15](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites#cite_note-17)

## IRIG

**IRIG Timecode** est une norme qui permet l'encodage et la transmission de l'[horodatage](https://fr.wikipedia.org/wiki/Horodatage "Horodatage"). Une norme d'encodage du [temps](https://fr.wikipedia.org/wiki/Temps "Temps") s'avère en effet nécessaire pour de nombreuses applications via réseau informatique, à des fins notamment de [synchronisation](https://fr.wikipedia.org/wiki/Synchronisation_d%27horloges "Synchronisation d'horloges"). Cette norme est ainsi utilisée pour les signaux vidéo, de [télémétrie](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9m%C3%A9trie_(informatique) "Télémétrie (informatique)"), de [radar](https://fr.wikipedia.org/wiki/Radar "Radar") et d'autres données captées.

IRIG est l'[acronyme](https://fr.wikipedia.org/wiki/Acronyme "Acronyme") pour l'**Inter-Range Instrumentation Group**, organisme américain établissant des [standards](https://fr.wikipedia.org/wiki/Norme_et_standard_techniques "Norme et standard techniques") dans le domaine des [signaux](https://fr.wikipedia.org/wiki/Signal_%C3%A9lectrique "Signal électrique") et du codage du temps.

### Origine

La norme IRIG Timecode est issue d'un projet de l'armée des États-Unis. Elle est publiée la première fois par le secrétariat du RCC (Range Commanders Council) pour White Sands Missile Range (WSMR) en 1956 et ensuite clôturée en 1960. Elle a été révisée à plusieurs reprises.

#### Caractéristiques techniques

À la différence de [PTP](https://fr.wikipedia.org/wiki/Precision_Time_Protocol "Precision Time Protocol") basé sur du câblage [Ethernet](https://fr.wikipedia.org/wiki/Ethernet "Ethernet"), IRIG implique l'utilisation d’un [câble coaxial](https://fr.wikipedia.org/wiki/C%C3%A2ble_coaxial "Câble coaxial") pour transmettre les informations de synchronisation.

#### Variantes et versions

IRIG Timecode est une norme assez ancienne (liste non complète) :

- IRIG 104-60 de 1960
- sIRIG 104-70
- IRIG 106-09 utilisé dans la télémétrie aéronautique, chapitre 10 pour les boîtes noires
- IRIG 200-70
- IRIG 200-98 (Mai 1998)
- IRIG 200-04 (Septembre 2004)
- IRIG 313-01 définition pour systèmes de test des capteurs, décodeurs en aéronautique

#### Sous-groupes

Dans IRIG, le code est réparti dans divers sous-groupes appelés A, B, D, E, G, H. Les différences entre les groupes sont entre autres le nombre des pulses de synchronisation par seconde ainsi que la modulation utilisée et le format des trames. Les différentes versions ne sont pas compatibles entre elles.

- IRIG B : est le plus fréquemment utilisé
- IRIG H : [NIST](https://fr.wikipedia.org/wiki/National_Institute_of_Standards_and_Technology "National Institute of Standards and Technology") utilise ce codage pour les "NIST Time and Frequency services" (radio-émetteurs)

## Configuration de spectracom

Il faut faire le configuration de l'ip sur l'équipement

Remarque: il faut que l'antenne soit dehors vers le ciel pour que la synchronisation soit faite avec les satellite

### Configuration du ntpd

Stratum1

/etc/ntp.conf

```conf
driftfile /var/lib/ntp/drift
server @ip iburst
includefile /etc/ntp/crypto/pw
keys /etc/ntp/keys
```

`ntpstat`

`ntpq -p`
Remarque: si il y a un * devant le serveur c'est que le client est synchronisé avec le serveur
