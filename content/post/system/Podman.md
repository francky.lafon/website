---
title: "Podman"
description: "l'homme pod !"
date: "2023-02-12"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: podman.png
categories:
 - Système
 - Devops
---

## Introduction

Podman est un projet open source qui a été initialement développé par Red Hat en 2018 en réponse à la demande de clients pour une alternative légère et sécurisée à Docker. Les développeurs de Red Hat ont voulu créer un outil qui permettrait aux utilisateurs de gérer des conteneurs sans nécessiter un démon en arrière-plan ou une infrastructure complète, tout en offrant des fonctionnalités équivalentes à celles de Docker.

Podman permet de gérer des conteneurs, des images et des répertoires, tels que le téléchargement, la création, la modification et la suppression de conteneurs et d'images. Il offre également des fonctionnalités telles que l'exécution en tant que processus, le partage de répertoires avec des conteneurs, la gestion des réseaux, la génération de conteneurs à partir de scripts, etc.

L'utilisation de Podman est simple et semblable à celle de Docker, avec des commandes telles que podman run, podman ps et podman images pour gérer des conteneurs et des images. Les conteneurs exécutés avec Podman sont totalement isolés les uns des autres et du système hôte, ce qui les rend sécurisés et fiables pour les déploiements en production.

En utilisant Podman, les administrateurs système peuvent facilement gérer les conteneurs sans nécessiter un démon en arrière-plan ou une infrastructure complète pour le faire. Cela peut être particulièrement utile pour les environnements qui ne nécessitent pas de déploiements complexes de conteneurs, mais qui souhaitent tout de même en utiliser pour améliorer la flexibilité et la portabilité des applications.

En 2020, Podman a été choisi par la fondation Linux pour être inclus dans la distribution Linux standard. Cela a permis à Podman de devenir un élément clé de la plupart des distributions Linux courantes, ce qui en a fait un choix populaire pour les développeurs et les administrateurs système soucieux de la sécurité et de la facilité d'utilisation.

## Les différences entre docker et Podman

Il existe plusieurs différences entre Docker et Podman, notamment:

Architecture: Docker utilise une architecture client-serveur, tandis que Podman est un outil en ligne de commande qui exécute directement des commandes sur le système hôte.

Administration: Docker nécessite un daemon en arrière-plan pour fonctionner, ce qui peut nécessiter une administration supplémentaire pour assurer la stabilité et la sécurité du système. Podman, en revanche, est un outil en ligne de commande simple qui n'utilise pas de daemon.
Fichiers de données: Docker utilise un système de fichiers de données dédié pour les conteneurs, tandis que Podman utilise des répertoires sur le système hôte.

Fonctionnalités: Docker offre un ensemble complet de fonctionnalités pour la gestion des conteneurs, telles que la gestion des réseaux, la gestion des volumes de données, etc. Podman, en revanche, se concentre sur la gestion des conteneurs et offre moins de fonctionnalités que Docker.

Sécurité: Docker utilise une sécurité basée sur des conteneurs et utilise des capacités pour isoler les conteneurs les uns des autres. Podman, en revanche, utilise des namespaces pour fournir une isolation plus stricte et une sécurité accrue pour les conteneurs.

Interopérabilité: Docker utilise un format de conteneur propriétaire (Docker Image Format) qui n'est pas compatible avec d'autres systèmes de conteneurs, tandis que Podman utilise des images de conteneur standardisées (Open Container Initiative) qui peuvent être utilisées avec d'autres systèmes de conteneurs.

En fin de compte, le choix entre Docker et Podman dépend de vos besoins en matière de fonctionnalités, de sécurité et d'interopérabilité. Si vous avez besoin d'un ensemble complet de fonctionnalités pour gérer vos conteneurs et que la sécurité n'est pas une préoccupation majeure, Docker peut être la meilleure option. Si vous avez besoin d'une sécurité accrue et d'une interopérabilité accrue avec d'autres systèmes de conteneurs, Podman peut être la meilleure option.

## Les commandes

```bash
podman run # utilisé pour lancer un conteneur.
podman ps # affiche une liste de tous les conteneurs en cours d'exécution.
podman images # affiche une liste de toutes les images disponibles localement.
podman pull # utilisé pour télécharger une image depuis un dépôt distant.
podman create # utilisé pour créer un conteneur à partir d'une image.
podman start # utilisé pour démarrer un conteneur arrêté.
podman stop # utilisé pour arrêter un conteneur en cours d'exécution.
podman rm # utilisé pour supprimer un ou plusieurs conteneurs.
podman rmi # utilisé pour supprimer une ou plusieurs images.
podman inspect # utilisé pour inspecter les détails d'un conteneur ou d'une image.
podman network connect #  Pour connecter un conteneur à un réseau existant
podman network disconnect #  Pour déconnecter un conteneur d'un réseau,
podman network ls # Pour afficher les informations sur les réseaux existants
```

## Création des images avec Buildah

Buildah est un outil en ligne de commande pour construire des images de conteneur sans utiliser de daemon. Il fournit une interface en ligne de commande pour construire, modifier et publier des images de conteneur en utilisant les différentes étapes de construction décrites dans un fichier Dockerfile ou dans des commandes en ligne de commande.

Buildah est conçu pour être plus flexible, plus rapide et plus sécurisé que Docker en ce qui concerne la construction d'images de conteneur. Il ne nécessite pas de daemon en arrière-plan pour fonctionner et peut donc être utilisé sur des systèmes d'exploitation sans support pour un daemon de conteneur, tels que les systèmes d'exploitation de serveur ou de cluster de calcul.

Buildah peut également être utilisé pour construire des images de conteneur qui sont compatibles avec d'autres systèmes de conteneur, tels que Podman, Kubernetes, etc. Les images de conteneur construites avec Buildah peuvent être stockées localement ou publiées sur des dépôts d'images tels que Docker Hub.

En résumé, Buildah est un outil pratique pour les développeurs et les opérateurs qui souhaitent construire, modifier et publier des images de conteneur de manière plus flexible, plus rapide et plus sécurisée que les méthodes traditionnelles.

```bash
buildah bud # construit une image à partir d'un fichier de configuration.
buildah from # définit l'image de base à utiliser pour la construction.
buildah run # exécute une commande dans un conteneur temporaire.
buildah copy # copie des fichiers ou des dossiers dans un conteneur.
buildah add # ajoute des fichiers ou des dossiers à un conteneur.
buildah mount # monte un conteneur dans un système de fichiers local.
buildah unmount # démonte un conteneur du système de fichiers local.
buildah config # définit les paramètres du conteneur, tels que le nom, l'étiquette, l'environnement, etc.
buildah commit # enregistre les modifications apportées à un conteneur en tant qu'une nouvelle image.
buildah images # affiche la liste des images disponibles localement.
buildah inspect # affiche les détails sur une image ou un conteneur.
buildah delete # supprime une image ou un conteneur.
```
