---
title: "Formation Terraform"
description: ""
date: "2023-02-17"
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: "xfce.jpg"
draft: false
categories:
 - Système
---


## Tradition IT & challenge

Buisiness -> buisness analyst -> solution architect -> Infra Team

résultat

- slow deployment
- expensive
- limited automation
- human error
- wasted ressources
- inconsistency

## Type of IaC tool

-> Configuration management

- Ansible
- Puppet
- Saltstack

advantage

- designed to install and manage software
- maintain standard structure
- version contrôle
- idempotent

-> Server template's

- Docker
- Packer
- Vagrant

Advantage

- pre installed software and dependency
- virtual machine or docker image
- immutable infra

-> provisioning tools

- Terraform
- Cloud formation

Advantage

- deploy immutable infra ressource
- server, databases

## Why Terraform ?

it's a declarative language
-> init
-> plan
-> apply

Installation
resource ??

## Langage hcl2

``` hcl2
<block> <parameters> {
    key1= values1
}
```

Example of ressources

``` hcl2
resource "local_file" "pet"{
    filename: "/root/cats.txt"
    content:"we love pets"
}
```

## Default file

main.tf:  ressource definition
variable.tf:  var declaration
output.tf:  contain output from ressource
providers.tf: contain provider definition

 .tfstate:  très important c'est qui est en prod

## Variable block

Best practice it's to declare the type of the variable.

``` hcl2
variable "toto"{
    default
    type  string | number | bool | any | list | map | object | tuple
    description
}
```

chapitre 26 a 30 à tester

## Commande line

`tf show` pour voir ce qui a en place
`tf plan`: pour update faire un
`tf output`
`tf refresh`
`tf validate`
`tf fmt`
`tf graph` (le output est imbuvable il faut un soft graphviz) ...
`tf graph | dot -Tsvg > graph.svg`
`tf state show` list | mv | rm pull | show
`tf untaint`: quand tu fais des modifications manu et tu veux pas que le tf pe
te
`tw workspace`:

## Lifecycle

I have 3 mode of deployment:

- create_before_destroy
- prevent_destroy
- ignore_changes

``` hcl2
resource "local_file" "pet"{
    filename: "/root/cats.txt"
    content:"we love pets"
    lifecycle {
        create_before_destroy = true
        prevent_destroy = true
        ignore_changes =[
           tags
           ignore_changes =all
        ]
    }
}
```

## Datasources

ressource=data
create, update,destroy=reads
managed ressources= data ressource

meta-argument ??

count

``` hcl2
variable "filename "{
    default= [
      toto
      tata
      titi
    ]
}
```

``` hcl2
resource "local_file" "pet"{
    filename= "var.filename[count.index]"
    cout = lenght(var.filename)
}
```

for-each

``` hcl2
variable "filename "{
    type=string
    default= [
      toto
      tata
      titi
    ]
}
```

``` hcl2
resource "local_file" "pet"{
    filename=each.value

    for_each = toset(var.filename)
}
```

AWS

...

## Remote state

state lock
si le tfstate est dans le github. n'est pas bon
il faut le mettre dans le s3

```hcl2
terraform {
    backend "s3" {
    bucket =
    key = test.terraform.tfstate
    region =
    dynamodb_table =
    }
}
```

## Logging

export TF_LOG = info |  warning | error | debug | trace

export TF_LOG_PATH = /tmp/toto.log

Terraform import avec Ansible ??

## Module

A voir pour le lab

Workspace
parfait pour le lab
