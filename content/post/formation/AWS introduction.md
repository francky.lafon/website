---
title: "Introduction AWS"
description: "Description"
date: 2023-02-12
draft: true
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: image.
categories:
 - Cloud
---

## overview of aws

AWS, ou Amazon Web Services, est un leader mondial en matière de cloud computing. Depuis 2006, il propose une variété de services d'infrastructure en nuage pour les entreprises. Ce qui a commencé comme un service de stockage de données en ligne s'est rapidement développé pour devenir un fournisseur de services de cloud computing complets, couvrant des domaines tels que le stockage, les bases de données, le réseau, l'analyse, l'IA, le développement d'applications, la cybersécurité, la migration de données, et plus encore.

Parmi les dates clés dans l'histoire d'AWS, citons :

2002 : lancement du premier service de stockage de données en ligne de Amazon, appelé Simple Queue Service (SQS).
2006 : lancement d'AWS en tant que service autonome avec treize services différents.
2012 : lancement d'Amazon Redshift, un service de stockage de données en masse.
2014 : lancement d'Amazon QuickSight, un service de visualisation de données.
2016 : lancement d'Amazon Rekognition, un service d'analyse d'images et de reconnaissance faciale.
2018 : lancement d'Amazon SageMaker, un service d'apprentissage automatique en nuage.

Voici quelques-uns des services AWS les plus populaires :

Amazon Simple Storage Service (S3) : un service de stockage de données en ligne de grande capacité.
Amazon Elastic Compute Cloud (EC2) : une plate-forme de cloud computing pour exécuter des applications et des charges de travail.
Amazon Relational Database Service (RDS) : un service de base de données en nuage compatible avec plusieurs types de bases de données.
Amazon Virtual Private Cloud (VPC) : une infrastructure de cloud computing privé pour déployer des applications et des services.
Amazon Route 53 : un service de gestion de noms de domaine qui relie les noms de domaine à des ressources en ligne telles que des applications web.
Cette introduction n'est qu'un aperçu des services AWS et de leur historique. Il y a beaucoup plus de services et de fonctionnalités proposés par AWS qui peuvent aider les entreprises à développer et à déployer des applications, des services et des infrastructures en nuage.

Compute :

- Amazon Elastic Compute Cloud (EC2)
- Amazon Elastic Container Service (ECS)
- Amazon Elastic Kubernetes Service (EKS)
- Amazon Lambda
- Amazon Lightsail
- AWS Elastic Beanstalk
- AWS Outposts
- AWS Serverless Application Repository

Storage :

- Amazon Simple Storage Service (S3)
- Amazon Elastic Block Store (EBS)
- Amazon S3 Glacier
- Amazon S3 Transfer Acceleration
- Amazon FSx for Windows File Server
- Amazon FSx for Lustre
- Amazon Elastic File System (EFS)

Database :

- Amazon Relational Database Service (RDS)
- Amazon DynamoDB
- Amazon Aurora
- Amazon Neptune
- Amazon ElastiCache
- Amazon Timestream
- AWS Database Migration Service
- Amazon Quantum Ledger Database (QLDB)
- Amazon Managed Apache Cassandra Service

Networking & Content Delivery :

- Amazon Virtual Private Cloud (VPC)
- Amazon CloudFront
- Amazon Route 53
- Amazon API Gateway
- Amazon Direct Connect
- AWS App Mesh
- AWS Global Accelerator

Developer Tools :

- AWS Cloud Development Kit (CDK)
- AWS CodeStar
- AWS CodeCommit
- AWS CodeBuild
- AWS CodeDeploy
- AWS CodePipeline
- AWS X-Ray
- AWS Cloud9

Management & Governance :

- AWS Systems Manager
- AWS CloudFormation
- AWS CloudTrail
- AWS Config
- AWS Organizations
- AWS Control Tower
- AWS Auto Scaling
- AWS Service Catalog
- AWS Resource Groups

Security, Identity, & Compliance :

- AWS Identity and Access Management (IAM)
- Amazon Detective
- Amazon GuardDuty
- Amazon Macie
- AWS Certificate Manager
- AWS Key Management Service (KMS)
- AWS Directory Service
- AWS Single Sign-On (SSO)
- AWS Firewall Manager
- AWS Resource Access Manager (RAM)
- AWS Secrets Manager

Analytics :

- Amazon Redshift
- Amazon QuickSight
- Amazon EMR
- Amazon Kinesis
- Amazon Athena
- Amazon Managed Streaming for Apache Kafka (MSK)
- Amazon EventBridge

Artificial Intelligence :

- Amazon SageMaker
- Amazon Rekognition
- Amazon Polly
- Amazon Comprehend
- Amazon Lex
- Amazon Textract
- Amazon Translate
- Amazon Transcribe
- Amazon Forecast

Mobile Services :

- Amazon Pinpoint
- Amazon SNS
- Amazon Mobile Analytics
- AWS Amplify

AR & VR :

- Amazon Sumerian

Blockchain :

- Amazon Managed Blockchain

Business Applications :

- Amazon Connect
- Amazon Chime
- Amazon WorkDocs
- Amazon WorkMail

## Security

- AWS Key Management Service (KMS) : C'est un service de gestion de clés en nuage qui permet de créer et de gérer facilement des clés de chiffrement.

- AWS Certificate Manager (ACM) : C'est un service de gestion de certificats en nuage qui permet de gérer facilement les certificats SSL/TLS pour les applications web.

- AWS Directory Service : C'est un service de gestion de annuaires en nuage qui permet de déployer un annuaire en nuage pour les applications web.

- AWS Identity and Access Management (IAM) : C'est un service de gestion d'identités et d'accès en nuage qui permet de contrôler les accès aux ressources AWS.

- AWS Artifact : C'est un service de gestion de conformité en nuage qui permet de télécharger des attestations de conformité et des accords de niveau de service (SLA).

- AWS CloudFormation : C'est un service de modélisation et de déploiement en nuage qui permet de décrire et de déployer facilement des infrastructures AWS en utilisant des modèles de templates.

- AWS CloudTrail : C'est un service de journalisation en nuage qui permet de surveiller et d'enregistrer les activités AWS pour la conformité, la sécurité et la résolution des problèmes.

- AWS Config : C'est un service de gestion de configuration en nuage qui permet de surveiller et d'enregistrer la configuration des ressources AWS pour la conformité, la sécurité et la résolution des problèmes.

- AWS CloudWatch : C'est un service de surveillance en nuage qui permet de surveiller les performances, les ressources et les applications AWS pour la disponibilité, la performance et la sécurité.

- AWS Resource Groups : C'est un service de gestion de groupes de ressources en nuage qui permet de regrouper et de gérer des ressources AWS similaires pour une meilleure visibilité et une meilleure gestion.

mots clé de la formation

- IAM users
- IAM roles

AWS secret manager permet de

- gérer les secrets

- permet la rotation

- stock mot de passe, clé et token

exemple:

``` aws
import mysql.connector

connection = mysql.connector.connect(
    hosts='localhost'
    database=database='root'
    password=get_secret_value_reponse['secretString']
)
```

AWS Directory service

- Gère un Microsoft AD
- AD connector pour ce connecter à un AD.

## Compute

Voici la liste des services sur la partie compute:

- Amazon Elastic Compute Cloud (EC2) : C'est un service de calcul en nuage qui permet de déployer des applications et des services en toute élasticité. EC2 offre un grand choix d'instances, chacune conçue pour répondre à différents besoins en matière de performance et de coûts.

- Amazon Elastic Container Service (ECS) : C'est un service de gestion de containers qui facilite la déploiement, la mise à l'échelle et la gestion des applications en containers.

- Amazon Elastic Kubernetes Service (EKS) : C'est un service de gestion de clusters Kubernetes qui facilite la déploiement et la gestion des applications distribuées sur Kubernetes.

- Amazon Lambda : C'est un service de traitement sans serveur qui permet d'exécuter du code sans avoir à gérer des serveurs.

- Amazon Lightsail : C'est un service de déploiement facile d'applications web qui offre une gestion simple de serveurs, de bases de données, de stockage et de réseau.

- AWS Elastic Beanstalk : C'est un service de déploiement d'applications web qui facilite la déploiement, la mise à l'échelle et la gestion des applications web.

- AWS Outposts : C'est une infrastructure en nuage locale qui permet d'exécuter des applications et des services sur des équipements physiques localisés dans des installations client.

- AWS Serverless Application Repository : C'est un référentiel de applications sans serveur prêtes à l'emploi qui peuvent être déployées sur AWS Lambda.

## Storage

## Database

## Networking

## Management and governance

## Machine learning

## Architecture example
