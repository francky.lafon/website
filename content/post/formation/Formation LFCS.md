---
title: "LFCS"
description: "LFCS"
date: "2023-02-20"
draft: false
author: "Franck Lafon"
license: "CC BY-NC-ND"
image: "ansible.jpg"
categories:
 - Système
---

<https://training.linuxfoundation.org/certification/linux-foundation-certified-sysadmin-lfcs/>

Upcoming Program Changes (estimated late November 2022)

## Essential Commands (25%)

### Search for files

``` bash
find path . -name '*.jpg'  # Cherche avec l'extension .jpg
find -size +0m             # Cherche avec le poids
find -mmin -1              # Cherche un fichier modifié dans la dernière minutes
find -name Felix           #
find -iname Felix          #
find -mmin                 # modified minute (5,-5,+5)
find -mtime 2              # (24 hour periods)
find -size +-              # c=bytes k=kilobytes M=Megabytes G=Gigabytes
find -name "f*" -size 512k # And operator
find -name -o "f*"         # OR operator
find -name -not "f*"       # NOT operator
find -perm 664             # with exactly 664 permission
find -perm -664            # at least 664 permission
find -perm /664            # with any of these permission
find \! -perm -o=r         #
find -perm /u=r,g=r,o=r    #
```

### Evaluate and compare the basic file system features and options

### Compare and manipulate file content

Pour la comparaison de fichier

``` bash
# Pour la comparaison de fichier
cat | tac
tail -n 100 # 100 dernière ligne
head -n 100 # 100 première ligne

# sed - stream editor
sed    's/canda/usa/g' toto.txt
sed -i 's/canda/usa/g'             # -in-place

# cut - remove sections from each line of files
cut -d ' ' -f1 userinfo.txt        # Délimiteur et fields

# sort - sort lines of text files
# cq - report or omit repeated lines
sort toto
sort file | uniq


# diff - compare files line by line
diff -c             # context ?
diff -y             # side-by-side
```

### Use input-output redirection (e.g. >, >>, |, 2>)

- stdin 0: standard input
- stdout 1: standard output
- stderr 2: standard error

``` bash
>   # output of a command can be routed
>>  # using the same file for additional output
<   # input is used when feeding file content to a file
2>  # error on file
```

### Analyze text using basic regular expressions

``` bash
grep toto *   # Cherche dans le dossier toto
grep -r 'c.t' # Cherche de tout les dossiers
grep ^toto    # Cherche une ligne commence par
grep toto$    # Cherche une ligne qui fini par
grep \        # escape special caractère
grep *        # match the previous caractère
grep 0+  ??
```

### Archive, backup, compress, unpack, and uncompress files

### Create, delete, copy, and move files and directories

``` bash
ls      # list
ls -alh #
ls -Z   # contexte de sécurité
ls -n   # pour voir les uid
pwd     # print working directory
cd      # change directory
cd -    # go to previous directory
touch   # create file
mkdir   # make directory
cp      # copy
mv      # move
rm      # remove
```

### Create and manage hard and soft links

Pour avoir toutes les informations liés d'un fichier il y a la commande: `stat fichier`

TODO: faire un description des inodes

#### hard links

![hard links](screenshot_09-45-48.png)

only hard link to files, not folders
only hard link to files on the same filesystem

#### soft links

![soft links](Capture_d%E2%80%99%C3%A9cran_18-19-15.png)

### List, set, and change standard file permissions

user group other

- rwx rwx rwx

d - directory
/- regular file
c - caractère device
l - link
s - socket file
p - pipe
b - block device

remarque: lecture de droit de gauche à droite

revoir le `chmod =` un intérêt

``` bash
chgrp                    # change group
groups                   # show groupe d'un user
chown                    # change owner file/folder

chmod                    # change mode
chmod u+rw,g+rw          # pour faire plusieurs group
chmod 4664 suidfile      # rwS --- --- suidfile executable
chmod 4764 suidfile      # rws --- --- suidfile not execute
chmod 2664 sgidfile      # --- rwS --- sgidfile executable
chmod 2764 sgidfile      # --- rws --- sgidfile not execute
chmod 6664 both          # rwS rwS --- both not execute
chmod 1777` / `chmod -t` # pour le stickybit
```

### Read, and use system documentation

`ls --help`

```bash
man          # manual pages
man man      # le man du man
man 1 printf
man 3 printf
```

option du man

- 1 Programmes exécutables ou commandes de l'interpréteur de commandes (shell)
- 2 Appels système (fonctions fournies par le noyau)
- 3 Appels de bibliothèque (fonctions fournies par les bibliothèques des programmes)
- 4 Fichiers spéciaux (situés généralement dans /dev)
- 5 Formats des fichiers et conventions. Par exemple  /etc/passwd
- 6 Jeux
- 7 Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7), man-pages(7)
- 8 Commandes de gestion du système (généralement réservées a root)
- 9 Sous-programmes du noyau [hors standard]

``` bash
# apropos - director searching for commands
apropos director
apropos -s1,8 director

whatis
```

### Manage access to the root account

``` bash
sudo --login
su -
passwd --unlock root
```

## Operation of Running Systems (20%)

### Boot, reboot, and shut down a system safely

### Boot or change system into different operating modes

### Install, configure and troubleshoot bootloaders

### Diagnose and manage processes

TO DO: voir a quoi correspond les colonnes des commandes ps et top

``` bash
ps                 #
ps aux             #
ps ef              #
top                #
ps 1               #
ps -U user         #
pgrep -a syslog    #
nice -n 11 bash    #
renice 7 pid       #
ps lax             #
ps fax             #
kill -L            #
sighup             # restarting
sleep 180          #
command &          # exécuté la commande en arrière-plan
fg                 # pour le faire revenir le processus sur le terminal
jobs               #
bg pid             #
lsof -p pid        #
lsof /var/log/messages  #
```

ctrl + Z background

### Locate and analyze system log files

rsyslog - rocket-fast system for log processing

``` bash
grep ssh /var/log
tail -f fle
which sudo
journalctl /bin/sudo
journalctl -u sshd.service
journalctl
journalctl -e
journalctl -f
journalctl -p info | err | warning | crit
journalctl -p info -g '^b'
journalctl -S 02:00
journalctl -S 01:00 -U 02:00
journalctl -S '2021-11-16 12:04:55'
journalctl -b 0
last
lastlog
```

### Schedule tasks to run at a set date and time

`crontab`
match all possible value *
match multiple value = , (15,45)
range of value - (2-4)
path de la commande

- e -u
- r remove
- r -u user

`cp test /etc/cron.hourly/`

`anacron`
`/etc/anacron`
`anacron -T` pour tester le fichier de conf

`at 15:00`
`at 'August 20 2022'`
`at 2:30 August 20 2022`
`at 'now + 30 minutes'`

commande qui n'existe pas
`atq` ??
`atrm 20` ??

Les logs de cron sont dans: `/var/log/cron`

`anacron -n` (now)

`at 'now +1 minute'`
`echo 'toto test' | systemd-cat --identifier=at_scheduled_backup`
`sudo grep atd /var/log/cron`

`journalctl | grep at_scheduled backup`

### Verify completion of scheduled jobs

### Update software to provide required functionality and security

``` bash
dnf check-upgrade
dnf upgrade
dnf repolist
dnf repolist -v
dnf repolist --all
dnf config-manager --enable powertools
dnf config-manager --disable powertools
dnf config-manager --add-repo
dnf search
dnf info
dnf reinstall
dnf remove
dnf group list
dnf group list --hidden
dnf install ./toto.rpm
dnf autoremove
dnf history
dnf provides /etc/anacrontab
dnf repoquery --list nginx | grep conf
```

### Verify the integrity and availability of resources

``` bash
df -h
du -sh
free -h
uptime
lscpu
lspci
xfs_repair -v /dev/sdb1
fsck.ext4 -v -f -p /dev/sdb1
systemctl list-dependencies
```

### Verify the integrity and availability of key processes

### Change kernel runtime parameters, persistent and non-persistent

``` bash
sysctl -a
sysctl -w                  # temporaire
/etc/sysctl.d/.conf
sysctl -p /etc/sysctl.d/toto.conf
```

### Use scripting to automate system maintenance tasks

``` bash
touch script.sh
date >> /tmp/script.log
chmod +x script.sh
```

### Manage the startup process and services (In Services Configuration)

init - initialization system

- service
- socket
- device
- timer

man systemd.service

``` bash
systemctl edit -full sshd ???
systemctl revert sshd     ???
systemctl start service
systemctl stop service
systemctl reload service
systemctl restart service
systemctl disable service
systemctl is-enabled service
systemctl enabled service
systemctl enable|disable --now service
systemctl mask            ???
systemctl unmask          ???
systemctl list-units --type service --all
```

### List and identify SELinux/AppArmor file and process contexts

`ls -Z`

- unconfined_u user
- object_r role
- user_home_t type
- s0 level

1. only certain users can enter certain roles and certain  types
2. it lets authorized users and process do their job, by granting the permissions they need
3. authorized set of actions
4. everything else is denied

`ps axZ`

`semanage login -l`

`getenforce enforcing | permissive | disabled`

### Manage Software

### Identify the component of a Linux distribution that a file belongs to

## User and Group Management (10%)

### Create, delete, and modify local user accounts

Les informations sur les utilisateurs sont dans les fichiers suivants:

- /etc/passwd
- /etc/group
- /etc/shadow
- /etc/login.defs: Configuration control definitions for the login package
- /etc/skel

``` bash
# Créer un nouvel utilisateur ou modifier les informations par défaut appliquées aux nouveaux utilisateurs
useradd
useradd --defaults
useradd -s shell -d homedir
useradd --system            # créer un user system (uid < 1000)
useradd --uid

userdel                     # delete a user

id
whoami                      # Qui suis-je ?

# Modifier un compte utilisateur
usermod --move-home
usermod --login        # renommage du user
usermod --shell
usermod --lock
usermod --unlock
usermod --expiredate 2021-12-10

# set a password for a user
passwd

# Politique mot de passe
chage --lastday 0 jane
chage -1
chage --maxdays 30 jane
chage --list jane
```

### Create, delete, and modify local groups and group memberships

`groupadd dev`
`gpasswd --add john dev`
`groups john`
`gpasswd -d dev john`

`usermod -g dev john`
`usermod -g groupe principal`
`usermod -G groupe secondaire`

`groupmod --new-anem programs dev (-n)`

`groupdel programs`
`usermod --gid john john`
`groupdel programs`

### Manage system-wide environment profiles

`ls -a /etc/skel`

`printenv=env`
`HISTSIZE=20000`

`sudo nano -w /etc/hosts`

`/etc/environment`

`/etc/profile.d/lastlogin.sh`
`echo "tour last login was at: "> $HOME/lastlogin`

### Manage template user environment

### Configure user resource limits

`/etc/security/limits.conf`
hard limit c'est 30 process
soft c'est 20 process

nproc  30
fsize 1024 (kilobyte)
cpu 1

trinity  - nproc 3

`ulimit -a` ??

### Manage user privileges

`gpasswd -a trinity wheel`
`gpasswd -d trinity wheel`

`visudo` - permet d’éditer le fichier `/etc/sudoers`

Remarque: les users qui sont dans le groupe %wheel groupe sudo

- all host
- (all) run as user
- all  command

`sudo -u trinity ls /home/trinity`

``` bash
trinity ALL=(aaron,john) ALL
trinity ALL=(ALL) /bin/ls, bin/stat
trinity ALL=(ALL) NOPASSWD:/bin/ls, bin/stat
```

### Configure PAM

PAM - ??
`ls /etc/pam.d`

`ls /etc/pam.d/su`

`man pam.conf`

man pam...

## Networking (12%)

### Configure networking and hostname resolution statically or dynamically

``` bash
ip link show | ip l                        # affiche les interfaces
ip address   | ip a                        # affiche les addresses IP
ip route     | ip r                        # affiche les routes

cat /etc/resolv.conf
cat /etc/sysconfig/network-script/ifcfg`   # ...

nmcli device reapply enp0s3                # pour les modifications  ??
```

### Configure network services to start automatically at boot

``` bash
systemctl status NetworkManager
nmcli connection modify enp0s3 autoconnect yes ??
```

### Implement packet filtering

il y a plusieurs zone dans firewalld. La zone drop est le plus restrictif et la zone trust est la plus permissive.

``` bash
firewalld-cmd --info-service=cockpit
firewalld-cmd --add-source=10.11.12.0 --zone=trusted
```

### Start, stop, and check the status of network services

``` bash
ss -tulpn (l=listen t=tcp u=udp n=numeric values p=process)
netstat -tulpn
lsof -p process
```

### Statically route IP traffic

``` bash
ip route add 192.168.1.0/24 via 192.168.1.1 dev enp0s3
ip route del 192.168.1.1
ip route add default via @ip_gw
nmcli connection show
nmcli connection modify enp0s3 +ipv4.routes "192.168.0.0/24 10.0.0.110"
nmcli connection device reapply enp0s3
```

### Synchronize time using other network peers

``` bash
systemctl status chronyd
timedatectl status
timedatectl set-timezone Europe/Paris
systemctl set-ntp true
```

## Service Configuration (20%)

### Configure a caching DNS server

`dnf install bind bind-utils && systemctl start named && systemctl enable named && firewall-cmd --add-service=dns`

`/etc/named.conf`

```bash
listen-on port 53 {127.0.0.1; 192.168.1.1};
allow-query {localhost; 192.168.1.1};

listen-on port 53 {any};
allow-query {any}
```

`dig @localhost google.com`

### Maintain a DNS zone

ls /etc/named.conf

```bash
zone "example.com" IN {
    type master;
    file example.com.zone;
};
```

Les fichiers template sont dans le repertoire `/var/named/`

faire un fichier `/var/named/example.com.zone`
revoir la notions des types d'entrée DNS

```bash
TTL 1H
example.com. A 127.0.0.1
@  IN SOA @ administrator.exemple.com. (
                    0; serial
                    1D; refresh
                    1H; retry
                    1W; expire
                    3H); minimum
@      NS ns1.example.com.
@      NS ns2.example.com.
ns1    A  10.0.0.1
ns2    A  10.0.0.2
@      A  ??
www    A  203.65.45.23
www    CNAME 203.65.45.23
example.com MX 10 mail.example.com.
            MX 20 mail2.example.com.
mail    A 203.65.45.23
mail2   A 203.65.45.22
server  AAAA  2001:DB8:10::1
example.com TXT "hello world"
```

`systemctl restart named`

`dig @localhost exemple.com ANY`

### Configure email aliases

`dnf install postfix && systemctl start postfix && systemctl enable postfix`

`sendmail aaron@localhost <<< "Test mail"`

`cat /var/spool/mail/aaron`

`/etc/aliases`

``` bash
contact:aaron,john,jane
advertising: aaroon@somewebsite.com
/var/spool/mail/aaron
```

`newaliases` ??

### Configure SSH servers and clients

`/etc/ssh/sshd_config`
`man sshd_config`

pour faire des exceptions pour un users

```bash
AddressFamily any inet inet6
X11forwarding no
Match user aaron
  PasswordAuthentification yes
```

`~/.ssh/config` (chmod 600)
`~/.ssh/authorized_keys` (chmod 600)
`~/.ssh/known_hosts`

`ssh-keygen`
`ssh-copy-id`
`ssh_config.d/99-our-settings.conf` pour setup le ssh ...

### Restrict access to the HTTP proxy server

a voir ...

`dnf install squid`

`/etc/squid`

```bash
acl localnet src 192.168.1.0/24
acl external src 203.0.113.0/24
acl SSL_ports 443
```

### Configure an IMAP and IMAPS service

`dnf install dovecot systemctl start dovecot && systemctl enable dovecot`

`/etc/dovecot/dovecot.conf` - fichier de configuration

```bash
protocols = imap
listen = *,::
```

`/etc/dovecot/conf.d/...`
`10-master.conf` - pour la configuration des ports

revoir la liste des ports mails ...

`/etc/dovecot/conf.d/10-mail.conf`

```bash
mail_location = mbox:~/mail:INBOX=/var/mail/%u
```

`/etc/dovecot/conf.d/10-ssl.conf`

```bash
ssl = required
```

### Query and modify the behavior of system services at various operating modes

### Configure an HTTP server

a voir sur web ou/et DA ...

`dnf install httpd`

`vim /etc/httpd/conf/httpd.conf`

### Configure HTTP server log files

à revoir

```bash
ErrorLog "/var/log/httpd/error2.log"
LogLevel warn
<IfModule log_config_module>
...
```

### Configure a database server

`dnf install mariadb && systemctl start mariadb && systemctl enable mariadb`

`mysql_secure_installation`         -

`mysql -u root`                    - connection à la BDD

`/etc/my.cnf.d/mariadb-server.cnf` - fichier de configuration du server

### Restrict access to a web page

pas suivi

### Manage and configure containers

`dnf install podman`

`vim /etc/containers/registries.conf`
`unqualified-search-registries=["docker.io"]` - commenter la ligne avec toute les registries et mettre cette ligne

`touch /etc/containers/nodocker` - change la commande podman en docker

`docker search nginx` - faire une recherche sur les registries
`docker rmi nginx:latest` - supprime l'image nginx
`docker run -d nginx` - démarre un container en mode détaché
`docker container list` - liste tout les containers
`docker stop container_id` - arrête un container
`docker ps --all`
`docker run -d -p 8080:80 --name mysite nginx` - container web avec le ports 80 ouvert sur 8080
`nc localhost 8080` - vérification

### Manage and configure Virtual Machines

`dnf install libvert`

`vim testmachine.xml`

```xml
<domain  type="qemu">
    <name>TestMachine</name>
    <memory unit="GiB">1</memory>
    <vcpu>1</vcpu>
    <os>
        <type arch='x86_64'>hvm</type>
    </os>
</domain>
```

`virsh define testmachine.xml`
`virsh undefine TestMachine --remove-all-storage`
`virsh help` - liste toutes les commandes
`virsh list --all`
`virsh start TestMachine`
`virsh reboot TestMachine`
`virsh reset TestMachine`
`virsh shutdown TestMachine`
`virsh destroy TestMachine`
`virsh autostart TestMachine`
`virsh autostart --disable TestMachine`
`virsh dominfo TestMachine`
`virsh setvcpus TestMachine 2 --config --maximum`
`virsh setvcpus TestMachine 2 --config`
`virsh setmaxmem TestMachine 2048M --config`

## Storage Management (13%)

### List, create, delete, and modify physical storage partitions

`lsblk` - voir toutes les partitions
`fdisk --list /dev/sda` - voir les partitions d'un disque
`cfdisk /dev/sdb`
    - select label type
        - gpt
        - dos
        - sgi
        - sun
    - create | delete | change type partition (gui)

### Manage and configure LVM storage

`man lvm`

`lvmdiskscan`

`pvcreate /dev/sdc /dev/sdd`

`pvs`

`vgcreate my_volume /dev/sdc /dev/sdd`

`pvcreate`

`vgextend my_volume /dev/sde`

`vgs`

`vgreduce my_volume /dev/sde`

`lvcreate --size 2G --name partition1 my_volume`

`vgs`

`lvcreate --size 2G --name partition2 my_volume`

`lvs`

`lvresize --extents 100%VG my_volume/partition1`

`lvreseize --size 2G my_volume/partition1`

`lvdisplay`

`mkfs.xfs /dev/my_volume/partition1`

`lvresize --resizefs --size 3g my_volume/partition1`

### Create and configure encrypted storage

``` bash
cryptsetup --verify-passphrase open --type plain /dev/vde mysecuredisk
mkfs.xfs /dev/mapper/mysecuredisk
mount /dev/mapper/mysecuredisk /mnt
cryptsetup close mysecuredisk
cryptsetup luksformat /dev/vde
cryptsetup luksChangeKey /dev/vde
cryptsetup open /dev/vde mysecuredisk
```

### Configure systems to mount file systems at or during boot

``` bash
ls /mnt                   # Vérifié si le dossier est vide
mount /dev/sdb1 /mnt      # montage de /dev/sdb1 dans le dossier /mnt
touch /mnt/toto           # Créé le fichier toto dans la partition mnt (Vérification de rw/ro)
umount /mnt               # Démonter le /mnt
vim /etc/fstab            # fichier ou il y a la configuration des partitions systèmes
systemctl daemon-reload   # to update systemd
blkid /dev/sdb1           # pour avoir le uuid
```


### Configure and manage swap space

`swapon --show`

`mkswap /dev/vdb3`

`swapon --verbose /dev/vdb3`

`swapoff`

`dd if=/dev/zero of=/swap bs=1M count=128 status=progress`

### Create and manage RAID devices

- RAID 0 straight
- RAID 1 Miroring
- raid 5
- RAID 10

`mdadm --zero-superblock /dev/sdc /dev/sdd /dev/sde`

`mdadm --create /dev/md0 --level=0 --raid-device=3 /dev/sdc /dev/sdd /dev/sde`

`mdadm --create /dev/md0 --level=1 --raid-devices=2 /dev/sdc /dev/sdd --spare-device=1 /dev/sde`

`mdadm --stop /dev/md0`

`mdadm --create /dev/md0 --level=1 --raid-devices=2 /dev/sdc /dev/sdd`

`mdadm --manage /dev/md0 --add /dev/vde`

`cat /proc/mdstat`

`mdadm --manage /dev/md0 --remove /dev/vde`

### Configure systems to mount file systems on demand

``` bash
dnf install autofs nfs-utils && systemctl start autofs
/etc/exports
/etc/auto.master
/etc/auto.shares
systemctl reload autofs
```

/- dans le fichier auto.master veux dire ...

### Create, manage and diagnose advanced file system permissions

`getfacl examplefile`
`setfacl --modify user:aaron:rw examplefile`
`setfacl --modify mask:r examplefile`
`setfacl --modify group:wheel:rw examplefile`
`setfacl --modify user:aaron:--- examplefile`
`setfacl --remove user:aaron examplefile`
`setfacl --remove group:wheel examplefile`
`setfacl --recursive -m user:aaron:rwx dir1/`
`setfacl --recursive --remove user:aaron dir1/`

`man chattr`
`chattr +a newfile`
`chattr -a newfile`
`chattr +i newfile`
`chattr -i newfile`
`lsattr`

### Setup user and group disk quotas for filesystem

`dnf install quota`

`/dev/sda /mybackups/ usrquota,grpquota`

`quotacheck --create-files --user --group /dev/sdb aquatoa.group aquota.user`
`quotaon /dev/sdb`

`fallocate --lenght 100M /mybackups/aaron/100Mfile`

`edquota --user aaron`

`quota --user aaron`

il peux avoir dépacement mais au bou de x jours delete

`fallocate --length 40M /mybackups/aaron/40Mfile`

`quota --edit-period`

### Create and configure file systems

`mkfs.xfs -L "BackupVolume" -i size=512 /dev/sdb1`
`xfs_admin -l /dev/sdb1`- retourne les information du volume
`xfs_admin -L "Backup_Volume" /dev/sdb1` - modifie le label

`mkfs.ext4 /dev/sdb1 -L "Backupvolume" -N 5000000 /dev/sdb2`
`tune2fs -l /dev/sdb2`
`tune2fs -L /dev/sdb2`

### filesystem features and options

``` bash
findmnt -h # Affiche l'aide et les options disponibles pour la commande.
findmnt -o # Permet de sélectionner les champs à afficher. Par exemple, "-o SOURCE,TARGET,FSTYPE" affichera uniquement les colonnes "SOURCE", "TARGET" et "FSTYPE".
findmnt -t # Permet de filtrer les points de montage en fonction du type de système de fichiers. Par exemple, "-t ext4" affichera uniquement les points de montage associés au système de fichiers "ext4".
findmnt -n # Désactive la résolution de noms d'hôtes pour les points de montage réseau.

mount -o ro,noexec,nosuid
```
