---
title: "vault hashi"
description: "Vault Hashi"
date: "2023-02-17"
author: "Franck Lafon"
license: "CC BY-NC-ND"
draft: false
image: ssh.png
categories:
 - Système
---

### What is vault ?

- manage secret and protect sensitive data
- provides a single source of secret for the both  humans and machines
- provides completes lifecycle management for secrets
  - eliminates secret sprawl
  - securely store any secret
  - provide gouvernance for access to secrets

### What is a secret ?

- anything your organization deems sensitive
- usernames and password
- certificates
- API keys
- encryption keys

How Vault Works

-> vault interface

- CLI
- API
- UI

-> Vault authentification
authentification by:

- username / password
-

Vault create a token

### Why Organization choose vault  ?

- AWS
- GCP
- Azure
- AD

### Benefits of Hasicorp Vault

- store long-lived static secret
- dynamically generate secret, upon request
- fully featured API
- identity-based access accross differents clouds and system
- provides encryption as a service
- act as root or intermediate certificate authority

### Use cases

- centralize the storage of secret
- migrate to dynamically generated secrets
- secure data with a centralized workflow for encryption operations
- automate the generation of x.509 certificates
- migrate to identity-based access

## Learning the vault architecure

## vault components

storage backends
configuration location for storage of vault data storage is defined in the main vault config file with desired pram all data is encrypted in transit TLS and at-rest using AES256 not all storage backends are created equal some support HA others have better tools for management there is only one storage backend per vault cluster

secret engines
vault components there are responsible for managing secrets for your organization secret engines can store, generate or encrypt data many secret engines connect to the other service to generate dynamic credentials on demand many secret engines cant be enabled and used as needed even multiple secret engines of the same type secret engines are enabled and isolated as a path all interactions are done directly with the path itself

auth methods
vault components tat perform authentification and manage identities responsible for assigning identity and policies to a user multiple auth methods can be enable depending on your case auth methods can be diff by humans vs system methods once auth , vault will issue a client token used to make all subsequent vault requests (r/w) the fundamental goal of all auth methods is to obtain a token each token has an associated policy (or policies ) and a TTL default auth method for a new vault deployment = tokens

audit devices
keeps detailed logs of all requests and reponses to vault Audit log is formatted using JSON Sensitive information is hashed before logging can (and should) have more than one audit device enabled vault requires at least one audit device to write the log before completing the vault request -if enabled prioritize safety over availability
